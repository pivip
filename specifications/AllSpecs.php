<?php

/**
 * Load PHPSpec
 *
 * @link http://www.phpspec.org/ The PHPSpec homepage
 */
require_once 'PHPSpec.php';

set_include_path('../project/' . PATH_SEPARATOR . '../project/library' . PATH_SEPARATOR . get_include_path());

require_once 'Zend/Loader.php';
Zend_Loader::registerAutoload();

$options = new stdClass;
$options->recursive = true;
$options->specdocs = true;
$options->reporter = 'html';

PHPSpec_Runner::run($options);