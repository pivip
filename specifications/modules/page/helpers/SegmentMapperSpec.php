<?php

require_once 'modules/page/helpers/SegmentMapper.php';

class DescribeSegmentMapper extends PHPSpec_Context
{
	public function itShouldDefaultToTheContentSegment()
	{
		$this->pending();
	}

	public function itShouldSetTheContentSegmentForTheMainAction()
	{
		$this->pending();
	}

	public function itShouldMapSupportedActionsToTheirRespectiveSegments()
	{
		$this->pending();
	}
}