<?php

require_once 'modules/page/controllers/IndexController.php';

class DescribePageRetrieval extends PHPSpec_Context
{
	public function itShouldLoadAllBlocksOfACertainPage()
	{
		$this->pending();
	}

	public function itShouldFailSilentlyIfABlockIsUnavailable()
	{
		$this->pending();
	}

	public function itShouldLoadCommonBlocks()
	{
		$this->pending();
	}

	public function itShouldLoadBlocksPartOfAnAssociatedBundle()
	{
		$this->pending();
	}
}