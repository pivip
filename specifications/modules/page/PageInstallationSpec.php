<?php

require_once 'modules/page/Module.php';

class DescribePageInstallation extends PHPSpec_Context
{
	public function itShouldSayItIsInstalledIfTheDbTableExists()
	{
		$this->pending();
	}

	public function itShouldSayItIsNotInstalledIfNoDbTableIsPresent()
	{
		$this->pending();
	}

	public function itShouldCreateADbTable()
	{
		$this->pending();
	}

	public function itShouldRemoveTheDbTable()
	{
		$this->pending();
	}
}