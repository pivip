<?php

//require_once 'modules/openid/controllers/AuthenticationController.php';

class DescribeOpenidLoginForm extends PHPSpec_Context
{
	public function itShouldDisplayALoginFormInTheMenu()
	{
		$this->pending();
	}

	public function itShouldDisplayALoginFormInTheMainSection()
	{
		$this->pending();
	}

	public function itShouldDisplayALoginFormInTheSidebar()
	{
		$this->pending();
	}

	public function itShouldDisplayALoginFormInTheFooter()
	{
		$this->pending();
	}

	public function itShouldBeAbleToProcessALoginForm()
	{
		$this->pending();
	}
}