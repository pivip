<?php

require_once 'modules/openid/Module.php';

class DescribeOpenidAuthentication extends PHPSpec_Context
{
	public function itShouldCommenceLoginWhenALoginFormHasBeenSubmitted()
	{
		$this->pending();
	}

	public function itShouldHandleCommunicationErrors()
	{
		$this->pending();
	}

	public function itShouldAttemptToVerifyAResponseFromAProvider()
	{
		$this->pending();
	}

	public function itShouldHandleInvalidOpenids()
	{
		$this->pending();
	}

	public function itShouldDetectWhenASuperuserHasNotBeenDefinedYet()
	{
		$this->pending();
	}
}