<?php

require_once 'modules/filelogger/Module.php';

class DescribeFileLogging extends PHPSpec_Context
{
	public function itShouldAllowForConfigurationOfTheFileFormat()
	{
		$this->pending();
	}

	public function itShouldAllowForTheFilteringOfLogMessages()
	{
		$this->pending();
	}
}