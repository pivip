<?php

require_once 'modules/admin/controllers/ConfigController.php';

class DescribeAdministration extends PHPSpec_Context
{
	public function itShouldLinkToAllConfigurationOptionsInTheSidebar()
	{
		$this->pending();
	}

	public function itShouldDisplayAnOverviewOfAllModulesThatProvideOne()
	{
		$this->pending();
	}

	public function itShouldFallbackOnTheOverviewIfNoModuleIsSet()
	{
		$this->pending();
	}

	public function itShouldCallTheIndexActionOfModulesByDefault()
	{
		$this->pending();
	}

	public function itShouldCallSpecificActionsWhenSpecified()
	{
		$this->pending();
	}
}