<?php

require_once 'modules/admin/Abstract.php';

class DescribeConfigurationInterface extends PHPSpec_Context
{
	public function itShouldDenyAccessWhenTheVisitorHasInsufficientRights()
	{
		$this->pending();
	}

	public function itShouldNotProvideAnOverviewByDefault()
	{
		$this->pending();
	}
}