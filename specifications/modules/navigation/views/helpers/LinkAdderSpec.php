<?php

require_once 'modules/navigation/views/helpers/AddNav.php';

class DescribeLinkAdder extends PHPSpec_Context
{
	public function itShouldTurnParametersIntoLinks()
	{
		$this->pending();
	}

	public function itShouldAlwaysSetATitle()
	{
		$this->pending();
	}

	public function itShouldAcceptAdditionalAttributes()
	{
		$this->pending();
	}
}