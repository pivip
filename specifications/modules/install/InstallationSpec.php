<?php

require_once 'modules/install/Module.php';

class DescribeInstallation extends PHPSpec_Context
{
	public function itShouldSayItIsNotInstalledIfTheDbPasswordHasNotBeenChanged()
	{
		$this->pending();
	}

	public function itShouldSayItIsNotInstalledIfTheDbCannotBeConnectedWith()
	{
		$this->pending();
	}

	public function itShouldDetectAndAttemptToInstallAllAvailableModules()
	{
		$this->pending();
	}

	public function itShouldRemoveAllTablesRelatedToPivipFromTheDb()
	{
		$this->pending();
	}

	public function itShouldAttemptToStartTheInstallationIfPivipIsNotInstalled()
	{
		$this->pending();
	}
}