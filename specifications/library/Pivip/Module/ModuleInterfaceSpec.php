<?php

require_once 'library/Pivip/Module/Abstract.php';

class DescribeModuleInterface extends PHPSpec_Context
{
	public function itShouldMarkDependenciesAsMetWhenNoneAreSpecified()
	{
		$interface = new FakeAndUnrealModule();
		$this->spec($interface->checkDependencies())->should()->beTrue();
	}

	public function itShouldLowercaseDependencyNames()
	{
		$this->pending();
	}

	public function itShouldDetectUnmetDependencies()
	{
		$this->pending();
	}

	public function itShouldDetectMetDependencies()
	{
		$this->pending();
	}

	public function itShouldMarkDependenciesAsUnmetWhenJustOneIsUnmet()
	{
		$this->pending();
	}
}

class FakeAndUnrealModule extends Pivip_Module_Abstract
{
	protected $_installed = false;

	public static function isInstalled()
	{
		return $this->_installed;
	}

	public static function needsConfiguring()
	{
		return false;
	}

	public function install()
	{
		$this->_installed = true;
	}

	public function uninstall()
	{
		$this->_installed = false;
	}

	public static function bootstrap()
	{
	}
}

class FakeAndUnreal2Module extends FakeAndUnrealModule
{
	protected $_dependencies = array('FAkEandunrEAL' => array('min' => '0',
	                                                          'max' => '10'));
}