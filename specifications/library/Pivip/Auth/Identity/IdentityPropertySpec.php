<?php

require_once 'library/Pivip/Auth/Identity/Property.php';

class DescribeIdentityProperty extends PHPSpec_Context
{
	public function itShouldProvideAStringValueEvenIfSubpropertiesAreSet()
	{
		$subProperties = array('fake' => 'Xfce@rocks.org');
		$property = new Pivip_Auth_Identity_Property('email', $subProperties);
		$this->spec($property->value)->shouldNot()->beEmpty();
		$this->spec($property->value)->should()->beString();
	}

	public function itShouldProvideItsName()
	{
		$property = new Pivip_Auth_Identity_Property('nickname', 'Vinnl');
		$this->spec($property->name)->shouldNot()->beEmpty();
		$this->spec($property->name)->should()->beEqualTo('nickname');
	}

	public function itShouldLowerCaseThePropertyName()
	{
		$property = new Pivip_Auth_Identity_Property('NiCKnAMe', 'Vinnl');
		$this->spec($property->name)->should()->beEqualTo('nickname');
	}

	public function itShouldLowercaseTheTypeSubProperty()
	{
		$subProperties = array('type' => 'hOmE', 'value' => '+31234567890');
		$property = new Pivip_Auth_Identity_Property('tel', $subProperties);
		$this->spec($property->type)->should()->beString();
		$this->spec($property->type)->should()->beEqualTo('home');
	}

	public function itShouldDefaultEmailTypeToInternetWhenValueIsSetExplicitly()
	{
		$subProperties = array('value' => 'ilove@dut.ch');
		$property = new Pivip_Auth_Identity_Property('email', $subProperties);
		$this->spec($property->type)->should()->beEqualTo('internet');
	}

	public function itShouldDefaultEmailTypeToInternetWhenValueIsSetImplicitly()
	{
		$property = new Pivip_Auth_Identity_Property('email', 'ilove@dut.ch');
		$this->spec($property->type)->should()->beEqualTo('internet');
	}

	public function itShouldReturnAnEmptyStringForNonExistentSubproperties()
	{
		$property = new Pivip_Auth_Identity_Property('whatever', array());
		$this->spec($property->watskeburt)->should()->beEmpty();
		$this->spec($property->watskeburt)->should()->beString();
	}

	public function itShouldAcceptSubproperties()
	{
		$subProperties = array('fake' => 'Xfce@rocks.org');
		$property = new Pivip_Auth_Identity_Property('email', $subProperties);
		$this->spec($property->fake)->shouldNot()->beEmpty();
		$this->spec($property->fake)->should()->beString();
	}

	public function itShouldPublishAllSubproperties()
	{
		$subProperties = array('type' => 'fake', 'value' => 'Xfce@rocks.org');
		$property = new Pivip_Auth_Identity_Property('email', $subProperties);
		$publishedSubProperties = array();
		foreach($property as $subProperty => $value)
		{
			$publishedSubProperties[$subProperty] = $value;
		}
		$this->spec($publishedSubProperties)->should->beEqualTo($subProperties);
	}
}