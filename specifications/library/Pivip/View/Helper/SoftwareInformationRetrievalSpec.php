<?php

require_once 'library/Pivip/View/Helper/GetSoftwareInfo.php';

class DescribeSoftwareInformationRetrieval extends PHPSpec_Context
{
	public function itShouldReturnNullForUnknownInformation()
	{
		$helper = new Pivip_View_Helper_GetSoftwareInfo();
		$this->spec($helper->someUnknownInfo)->should->beNull();
	}

	public function itShouldReturnTheCorrectName()
	{
		$this->pending();
	}

	public function itShouldReturnTheCorrectIdentifier()
	{
		$this->pending();
	}

	public function itShouldReturnTheCorrectVersion()
	{
		$this->pending();
	}
}