<?php

require_once 'library/Pivip/Version/Abstract.php';
require_once 'library/Pivip/Version.php';

class DescribeVersionComparing extends PHPSpec_Context
{
	public function itShouldReturnANegativeNumberIfTheVersionIsOlder()
	{
		$result = Pivip_Version::compareVersion('0.2alpha', 'FakeAndUnreal');
		$this->spec($result)->should()->beLessThan(0);
	}

	public function itShouldReturnAPositiveNumberIfTheVersionIsNewer()
	{
		$result = Pivip_Version::compareVersion('0.2RC', 'FakeAndUnreal');
		$this->spec($result)->should()->beGreaterThan(0);
	}

	public function itShouldReturnZeroIfTheVersionIsEqual()
	{
		$result = Pivip_Version::compareVersion('0.2b', 'FakeAndUnreal');
		$this->spec($result)->should()->beEqualTo(0);
	}

	public function itShouldRecogniseCompatibleApisWithinItsRange()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'page',
		                                            '3.2');
		$this->spec($result)->should->beTrue();
	}

	public function itShouldRecogniseCompatibleApisOverItsRange()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'page',
		                                            '5.5', '3.0');
		$this->spec($result)->should->beTrue();
	}

	public function itShouldRecogniseCompatibleApisBelowItsRange()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'page',
		                                            '3.0', '0.0');
		$this->spec($result)->should->beTrue();
	}

	public function itShouldRecogniseIncompatibleApis()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'notpage',
		                                            '3.0');
		$this->spec($result)->should->beFalse();
	}

	public function itShouldRecogniseOutdatedApis()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'page',
		                                            '0.0.1');
		$this->spec($result)->should->beFalse();
	}

	public function itShouldRecogniseApisThatAreTooNew()
	{
		$result = Pivip_Version::checkCompatibility('FakeAndUnreal', 'page',
		                                            '5.1', '5.1');
		$this->spec($result)->should->beFalse();
	}

	public function itShouldBeAbleToRetrieveApiCompatibility()
	{
		$result = Pivip_Version::getApiCompatibility('FakeAndUnreal');
		$this->spec($result)->should->equal(FakeAndUnrealVersion::$api);
	}

	public function itShouldBeAbleToReturnCompatibilityWithOwnApi()
	{
		$api = array('name' => 'fakeandunreal2',
		             'min' => FakeAndUnreal2Version::getVersion(),
		             'max' => FakeAndUnreal2Version::getVersion()
		            );
		$result = Pivip_Version::getApiCompatibility('FakeAndUnreal2');
		$this->spec($result)->should->beEqualTo($api);
	}
}

class FakeAndUnrealVersion extends Pivip_Version_Abstract
{
	public static $api = array('name' => 'page',
	                           'min' => '0.0.3',
	                           'max' => '3.3.3');

	public static function getVersion()
	{
		return '0.2beta';
	}
}

class FakeAndUnreal2Version extends Pivip_Version_Abstract
{
	public static function getVersion()
	{
		return '0.2beta';
	}
}