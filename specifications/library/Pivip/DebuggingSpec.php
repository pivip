<?php

require_once 'library/Pivip/Debug.php';

class DescribeDebugging extends PHPSpec_Context
{
	public function itShouldDefaultTheSetupToProduction()
	{
		$this->pending();
	}

	public function itShouldReturnProductionForUnknownSetups()
	{
		$this->pending();
	}

	public function itShouldRecogniseADevelSetup()
	{
		$this->pending();
	}

	public function itShouldReturnTheDefinedSetup()
	{
		$this->pending();
	}

	public function itShouldOnlySayWeAreTestingWhenWeReallyAre()
	{
		$this->pending();
	}
}