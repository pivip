<?php

$base = './';

define('CODE_PATH', realpath($base) . '/');

set_include_path('library'
                 . PATH_SEPARATOR . CODE_PATH . 'modules'
                 . PATH_SEPARATOR . CODE_PATH . 'application/models'
                 . PATH_SEPARATOR . CODE_PATH . 'application/models/generated'
                 . PATH_SEPARATOR . get_include_path()
                 . PATH_SEPARATOR . CODE_PATH);

// Enable the auto-loading of classes that follow the ZF naming convention
// http://framework.zend.com/manual/en/coding-standard.naming-conventions.html
require 'Zend/Loader.php';
Zend_Loader::registerAutoload();

// Load the general configuration and save it to the registry
$generalConfig = new Vogel_Config_Ini(CODE_PATH . 'data/config/general.ini');
Zend_Registry::set('generalConfig', $generalConfig);

// Load the cache configuration and save it to the registry
$cacheConfig = new Vogel_Config_Ini(CODE_PATH . 'data/config/cache.ini');
Zend_Registry::set('cacheConfig', $cacheConfig);

$frontController = Zend_Controller_Front::getInstance();

// Make sure errors are only thrown when developing
if(Pivip_Debug::isTesting())
{
	error_reporting(E_ALL|E_STRICT);
	$frontController->throwExceptions(true);
	Doctrine::debug(true);
} else {
	error_reporting(0);
	$frontController->throwExceptions(false);
	Doctrine::debug(false);
}

$locale = new Zend_Locale();
$timezone = $locale->getTranslation($locale->getRegion(),
                                    'TimezoneToTerritory');
if(empty($timezone))
{
	$timezone = $generalConfig->locale->fallback->timezone;
}
date_default_timezone_set($timezone);

// Load a logger that can be hooked into
$writer = new Zend_Log_Writer_Null();
$logger = new Zend_Log($writer);
Zend_Registry::set('logger', $logger);

// If possible, connect to the database
try
{
	$options = $generalConfig->database->params->toArray();
	$dsn = Pivip_Utility_Doctrine::arrayToDsn($options);
	$connection = Doctrine_Manager::connection($dsn);
	// Enable the auto-loading of models
	spl_autoload_register(array('Doctrine', 'autoload'));
	Doctrine::loadModels(CODE_PATH . 'application/models');
	$manager = Doctrine_Manager::getInstance();
	$manager->setAttribute(Doctrine::ATTR_MODEL_LOADING, 'conservative');
	if(!empty($generalConfig->database->prefix))
	{
		$manager->setAttribute(Doctrine::ATTR_NAME_PREFIX,
		                       $generalConfig->database->prefix);
	}
} catch(Exception $e) {
	// Do nothing - not possible to connect to the database
}

Zend_Session::start();

// Register Pivip's view helpers
$viewRenderer =
	Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
$view = new Zend_View(array('encoding' => 'utf-8'));
$view->addHelperPath('Pivip/View/Helper', 'Pivip_View_Helper');
$view->addHelperPath('Vogel/View/Helper', 'Vogel_View_Helper');
$view->doctype('XHTML1_STRICT');
$viewRenderer->setView($view);

// Define the Access Control List
$acl = new Zend_Acl();
$acl->addRole(new Zend_Acl_Role('guest'))
    ->addRole(new Zend_Acl_Role('member'), 'guest')
    ->addRole(new Zend_Acl_Role('author'), 'member')
    ->addRole(new Zend_Acl_Role('editor'), 'author')
    ->addRole(new Zend_Acl_Role('moderator'), 'author')
    ->addRole(new Zend_Acl_Role('admin'), 'member')
    ->addRole(new Zend_Acl_Role('superuser'))
    ->add(new Zend_Acl_Resource('module'))
    ->allow('superuser');
Zend_Registry::set('acl', $acl);

// Load the modules and run their bootstrap code
$modules = new Pivip_Modules();

foreach($modules as $module)
{
	$module->bootstrap();
}

// With all modules set, start the dispatch loop
$frontController->addModuleDirectory('modules');
$frontController->dispatch();