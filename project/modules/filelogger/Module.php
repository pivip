<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_FileLogger
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the FileLogger module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Filelogger_Module extends Pivip_Module_Abstract
{
	/**
	 * Communicate that FileLogger is installed
	 *
	 * @return boolean True, because FileLogger does not need to be installed. 
	 */
	public static function isInstalled()
	{
		return true;
	}

	/**
	 * Communicate that FileLogger can accept configuration
	 *
	 * @return boolean True, the FileLogger can be configured.
	 */
	public static function needsConfiguring()
	{
		return true;
	}

	/**
	 * Load the module
	 *
	 * Add a file writer to the Zend_Log instance
	 */
	public function bootstrap()
	{
		try
		{
			$loggerConfig = new Zend_Config_Ini('modules/filelogger/config.ini');
			$writer = new Zend_Log_Writer_Stream($loggerConfig->logfile->path);

			if('simple' == $loggerConfig->logfile->format)
			{
				$format =
					'[%module%] %timestamp% %priorityName% (%priority%): %message%'
					. PHP_EOL;
				$formatter = new Zend_Log_Formatter_Simple($format);
			} else {
				$formatter = new Zend_Log_Formatter_Xml();
			}
			$writer->setFormatter($formatter);

			if(isset($loggerConfig->filter->priority))
			{
				$filter = new Zend_Log_Filter_Priority(
				          	$loggerConfig->filter->priority);
				$writer->addFilter($filter);
			}

			$logger = Zend_Registry::get('logger');
			$logger->addWriter($writer);
		} catch(Exception $e) {
			// Fail silently, no logging possible
		}
	}
}