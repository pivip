<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_ErrorHandler
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the ErrorHandler module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Errorhandler_Module extends Pivip_Module_Abstract
{
	/**
	 * Communicate that Errorhandler is installed
	 *
	 * @return boolean True, because Errorhandler does not need to be installed. 
	 */
	public static function isInstalled()
	{
		return true;
	}

	/**
	 * Communicate that ErrorHandler does not need any configuration
	 *
	 * Always returns false since there is nothing to configure for the
	 * ErrorHandler module.
	 *
	 * @return boolean False, since ErrorHandler does not need to be configured.
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Load the module
	 *
	 * Sets the error handler to the ErrorHandler module
	 */
	public function bootstrap()
	{
		$errorHandler = new Zend_Controller_Plugin_ErrorHandler
		                    (array('module' => 'errorhandler',
		                           'controller' => 'error',
		                           'action' => 'error'));
		Zend_Controller_Front::getInstance()->registerPlugin($errorHandler);
	}
}