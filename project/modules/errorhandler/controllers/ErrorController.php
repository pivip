<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_ErrorHandler
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Handle errors
 */
class Errorhandler_ErrorController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Catch errors 
	 */
	public function errorAction()
	{
		$errors = $this->_getParam('error_handler');
		if($errors->type == Zend_Controller_Plugin_ErrorHandler
		                    ::EXCEPTION_NO_CONTROLLER ||
		   $errors->type == Zend_Controller_Plugin_ErrorHandler
		                    ::EXCEPTION_NO_ACTION)
		{
			$translate = Zend_Registry::get('Zend_Translate');
			$this->_flashMessenger->addMessage($translate->_(
				'Some parts of the page could not be found.'));
		}
		$logger = Zend_Registry::get('logger');
		$logger->debug($errors->exception->getMessage() . "\n" .
		               $errors->exception->getTraceAsString());
		if(Pivip_Debug::isTesting())
		{
			Pivip_Debug::dump($errors);
		}
	}
}