<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Translate
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Translate module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Translate_Module extends Pivip_Module_Abstract
{
	/**
	 * Defines the dependencies
	 *
	 * Translate depends on Pivip
	 */
	protected $_dependencies = array('Pivip' => array('max' => '0.0.0dev'));

	/**
	 * Communicate that Translate is installed
	 *
	 * @return boolean True, because Translate does not need to be installed. 
	 */
	public static function isInstalled()
	{
		return true;
	}

	/**
	 * Communicate that Translate does not need configuration
	 *
	 * @return boolean False, since Translate does not need to be configured.
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Load the module
	 *
	 * Opens the cache for translations, loads the languages.
	 */
	public function bootstrap()
	{
		$translateConfig = new Zend_Config_Ini('./modules/translate/config.ini');
		Zend_Locale::setDefault($translateConfig->default);
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$options = $cacheConfig->toArray();
		$separator = '';
		if('/' != substr($options['cache']['cache_root'], -1))
		{
			$separator = '/';
		}
		$options['backendOptions']['cache_dir'] = $options['cache']['cache_root']
		                                           . $separator
		                                           . 'modules/translate';
		$frontendOptions = array('automatic_serialization' => true);
		$cache = Zend_Cache::factory('Page',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $options['backendOptions']);
		//Zend_Translate::setCache($cache);
		$translate = new Zend_Translate('gettext', 'data/translations', null,
		                                array('scan'
		                                      	=> Zend_Translate::LOCALE_DIRECTORY
		                                ));
		Zend_Registry::set('Zend_Translate', $translate);
		Zend_Form::setDefaultTranslator($translate);
	}
}