<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Imagestream
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Imagestream module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Imagestream_Module extends Pivip_Module_Abstract
{
	/**
	 * Check whether the Imagestream table exists
	 *
	 * @return boolean Whether the Imagestream module is installed 
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH
		                . 'application/models/generated/BaseImagestream.php'))
		{
			return false;
		}
		try
		{
			$imagestreamTable = Doctrine::getTable('Imagestream');
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * @return boolean False
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Install the Imagestream module
	 */
	public function install()
	{
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'imagestream');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Load the generic cache for use by this module
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache()
	{
		$frontendOptions = array('cache_id_prefix' => 'imagestream_',
		                         'automatic_serialization' => true);
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$cache = Zend_Cache::factory('Core',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $cacheConfig->backendOptions->toArray());
		return $cache;
	}

	/**
	 * @return array A list of the blocks this module provides for normal pages,
	 *               in the format:
	 *               array('section' => array('Link to add this block'
	                                          	=> 'Block name'))
	 */
	public static function getPageBlocks($location)
	{
		if(!self::isInstalled())
		{
			return array();
		}
		$translate = Zend_Registry::get('Zend_Translate');
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Imagestream_AddStream');
		$blocks = array();
		$sections = array('main');
		foreach($sections as $section)
		{
			$blocks[$section] = array($route->assemble(array('section' => $section,
			                                                 'location'
			                                                 	=> $location))
			                          	=> $translate->_('Image stream'));
		}
		return $blocks;
	}

	/**
	 * Load the module
	 */
	public function bootstrap()
	{
		// Define the Access Control List
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('imagestream'))
		    ->allow('admin', 'imagestream', 'write');
		Zend_Registry::set('acl', $acl);
		// Define the routes
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$options = array('module' => 'imagestream', 'controller' => 'stream',
		                 'action' => 'add', 'location' => '');
		$route = new Zend_Controller_Router_Route(
		         	'imagestream/add/:section/:location', $options);
		$router->addRoute('Imagestream_AddStream', $route);
		$options = array('module' => 'imagestream', 'controller' => 'stream',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route('imagestream/edit/:stream_id',
		                                          $options);
		$router->addRoute('Imagestream_EditStream', $route);
		$options = array('module' => 'imagestream', 'controller' => 'stream',
		                 'action' => 'delete');
		$route = new Zend_Controller_Router_Route('imagestream/delete/:stream_id',
		                                          $options);
		$router->addRoute('Imagestream_DeleteStream', $route);
	}
}