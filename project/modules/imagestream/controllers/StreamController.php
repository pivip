<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Imagestream
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Stream images from other websites
 */
class Imagestream_StreamController extends Page_Abstract
{
	const API_KEY = '285259122a01117b5446fd6f5a44b09f';

	/**
	 * Display the stream in the main section of the website
	 *
	 * @todo Cache the service search results.
	 */
	public function mainAction()
	{
		$streamId = $this->_request->getParam('block_id');
		if(null === $streamId)
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$cache = Imagestream_Module::loadCache();
		if(!$row = $cache->load($streamId))
		{
			$row = Doctrine::getTable('Imagestream')->find($streamId);
			$cache->save($row, $streamId, array('stream', 'block'));
		}
		switch($row->service)
		{
			case 'flickr':
				$service = new Zend_Service_Flickr(self::API_KEY);
				break;
			case '23':
			default:
				$service = new Vogel_Service_23(self::API_KEY);
				break;
		}
		$options = array('per_page' => 5);
		if(isset($row->amount))
		{
			$options['per_page'] = intval($row->amount);
		}
		switch($row->type)
		{
			case 'user':
				try
				{
					$this->view->images = $service->userSearch($row->keywords,
					                                           $options);
				} catch(Exception $e) {
					$logger = Zend_Registry::get('logger');
					$logger->err($e->getMessage());
					$this->_helper->viewRenderer->setNoRender();
					return;
				}
				break;
			case 'tags':
			default:
				try
				{
					$this->view->images = $service->tagSearch($row->keywords, $options);
				} catch(Exception $e) {
					$logger = Zend_Registry::get('logger');
					$logger->err($e->getMessage());
					$this->_helper->viewRenderer->setNoRender();
					return;
				}
				break;
		}
		switch($row->size)
		{
			case 'Square':
				// Break intentionally omitted
			case 'Thumbnail':
				// Break intentionally omitted
			case 'Small':
				// Break intentionally omitted
			case 'Medium':
				// Break intentionally omitted
			case 'Large':
				// Break intentionally omitted
			case 'Original':
				$this->view->imageSize = $row->size;
				break;
			default:
				$this->view->imageSize = 'Small';
		}
		$this->view->urlParams = array('stream_id' => $streamId);
		if($this->_isAllowed('edit'))
		{
			$this->view->allowEdit = true;
		}
		if($this->_isAllowed('delete'))
		{
			$this->view->allowDelete = true;
		}
	}

	/**
	 * Add a stream
	 */
	public function addAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$location = '/' . $this->_request->getParam('location');
		if(!$this->_isAllowed('add'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to add an image stream.'));
			$this->_redirect($location);
		}
		$section = $this->_request->getParam('section');
		if(empty($section))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You need to specify a section to add an image stream to.'));
			$this->_redirect($location);
		}
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		$form = $this->_getForm();
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$block = new Block();
			$block->location = $location;
			$block->action = $section;
			$block->controller = 'stream';
			$block->module = 'imagestream';
			$block->Imagestream->service = $form->getValue('service');
			$block->Imagestream->keywords = $form->getValue('keywords');
			$block->Imagestream->amount = intval($form->getValue('amount'));
			$block->Imagestream->type = $form->getValue('type');
			$block->Imagestream->size = $form->getValue('size');
			$block->save();
			$cacheId = Page_Module::urlToCacheId($location);
			$cache = Page_Module::loadCache();
			$cache->remove($cacheId);
			$this->_flashMessenger->addMessage($translate->_('Block added.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate
			                                   ->_('Failed to add the block.'));
		}
		$this->_redirect($location);
	}

	/**
	 * Edit a stream
	 */
	public function editAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');

		$streamId = $this->_request->getParam('stream_id');

		if(null === $streamId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No image stream specified.'));
			$this->_redirect();
		}

		$streamTable = Doctrine::getTable('Imagestream');
		$stream = $streamTable->find($streamId);

		if(!$this->_isAllowed('edit'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to edit image stream.'));
			$this->_redirect($html->Block->location);
		}

		$this->_helper->viewRenderer->setScriptAction('add');
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);

		$form = $this->_getForm();
		$form->submit->setLabel('Edit');
		$form->populate($stream->toArray());
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$stream->service = $form->getValue('service');
			$stream->keywords = $form->getValue('keywords');
			$stream->amount = intval($form->getValue('amount'));
			$stream->type = $form->getValue('type');
			$stream->size = $form->getValue('size');
			$stream->save();
			$this->_flashMessenger->addMessage($translate->_('Block updated.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate->_(
				'An error occured while updating the block, please try again.'));
			$this->_redirect($this->_request->getRequestUri());
		}
		$cache = Imagestream_Module::loadCache();
		$cache->remove($streamId);
		$this->_redirect($html->Block->location);
	}

	/**
	 * Delete a stream
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$streamId = $this->_request->getParam('stream_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $streamId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No image stream specified.'));
			$this->_redirect();
		}

		$streamTable = Doctrine::getTable('Imagestream');
		$stream = $streamTable->find($streamId);

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to delete blocks.'));
			$this->_redirect($stream->Block->location);
		}

		$location = $stream->Block->location;
		try
		{
			$stream->Block->delete();
			$stream->delete();
			$this->_flashMessenger->addMessage($translate->_(
				'Block deleted.'));
			$cache = Imagestream_Module::loadCache();
			$cache->remove($streamId);
			$cache = Page_Module::loadCache();
			$cache->remove(Page_Module::urlToCacheId($location));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'An error occurred while deleting the block, please try again.'));
		}
		$this->_redirect($location);
	}

	/**
	 * @return Zend_Form The form to add an HTML block
	 */
	protected function _getForm()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$form = new Zend_Form;
		$form->setMethod('post')
		     ->setAction($this->_request->getRequestUri())
		     ->addElement('select', 'service')
		     ->service->setRequired(true)
		              ->setLabel('Service')
		              ->addMultiOption('23', '23')
		              ->addMultiOption('flickr', 'Flickr');
		$form->addElement('text', 'keywords')
		     ->keywords->setRequired(true)
		               ->setLabel('Keywords')
		               ->addValidator('NotEmpty');
		$form->addElement('select', 'type')
		     ->type->setRequired(true)
		           ->setLabel('Search type')
		           ->addMultiOption('tags', $translate->_('Tags'))
		           ->addMultiOption('user', $translate->_('User'));
		$form->addElement('select', 'size')
		     ->size->setRequired(true)
		           ->setLabel('Image size')
		           ->addMultiOption('Square', $translate->_('Square'))
		           ->addMultiOption('Thumbnail', $translate->_('Thumbnail'))
		           ->addMultiOption('Small', $translate->_('Small'))
		           ->addMultiOption('Medium', $translate->_('Medium'))
		           ->addMultiOption('Large', $translate->_('Large'))
		           ->addMultiOption('Original', $translate->_('Original'))
		           ->setValue('Small');
		$form->addElement('text', 'amount')
		     ->amount->setValue(5)
		             ->setLabel('Amount');
		$form->addDisplayGroup(array('keywords', 'type'), 'stream')
		     ->stream->setLegend('Required options');
		$form->addDisplayGroup(array('service', 'amount', 'size'), 'advanced')
		     ->advanced->setLegend('Advanced options');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Add')
		       ->addDecorator('HtmlTag', array('tag' => 'dd'))
		       ->removeDecorator('DtDdWrapper');
		$form->addElement($submit);
		return $form;
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges = null)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if('edit' == $privileges || 'add' == $privileges ||
		   'delete' == $privileges)
		{
			if(!$acl->isAllowed('guest', 'imagestream', 'write')
			   && !$auth->hasIdentity())
			{
				return false;
			}
			if(!$acl->isAllowed($identity->aclRole, 'imagestream', 'write'))
			{
				return false;
			}
		}
		if(!$acl->isAllowed('guest', 'block', $privileges) &&
		   !$auth->hasIdentity())
		{
			return false;
		}
		return $acl->isAllowed($identity->aclRole, 'block', $privileges);
	}
}