<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Page
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Modules can extend this to provide their page blocks
 *
 * Page sections are:
 * 	(probably in header)
 * 	* logo
 * 	* nav
 * 	* quickview
 * 	* menu
 * 	(probably in body)
 * 	* notifications
 * 	* sidebar
 * 	* main
 * 	(probably in footer)
 * 	* aside
 * 	* links
 * 	* meta
 */
abstract class Page_Abstract extends Pivip_Controller_Module_Abstract
{
	/**
	 * Fallback when calling undefined actions
	 *
	 * De-activates view rendering, saves a log message about the action being
	 * called while not defined.
	 */
	public function __call($function, $arguments)
	{
	}
}