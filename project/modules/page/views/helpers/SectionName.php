<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Page
 * @subpackage ViewHelpers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Convert a section identifier to a readable name
 */
class Navigation_View_Helper_SectionName
{
	public $view;

	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 * Convert a section identifier to a readable name
	 *
	 * @param  string $section Section identifier.
	 * @return string The section name.
	 */
	public function sectionName($section)
	{
		switch($section)
		{
			case 'logo':
				return $this->view->translate('Page title');
			case 'nav':
				return $this->view->translate('Navigation area');
			case 'quickview':
				return $this->view->translate('Quick overview');
			case 'menu':
				return $this->view->translate('User menu');
			case 'notifications':
				return $this->view->translate('Notification area');
			case 'sitecontext':
				return $this->view->translate('Site-wide sidebar');
			case 'pagecontext':
				return $this->view->translate('Page-specific sidebar');
			case 'aside':
				return $this->view->translate('Additional footer content');
			case 'links':
				return $this->view->translate('Additional links');
			case 'meta':
				return $this->view->translate('Meta information');
				break;
			case 'main':
				// Break intentionally omitted
			default:
				return $this->view->translate('Main content area');
		}
	}
}