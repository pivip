<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Page
 * @subpackage ActionHelpers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Maps actions to their respective response segments
 */
class SegmentMapper extends Zend_Controller_Action_Helper_Abstract
{
	/**
	 * Called before an action is called
	 *
	 * Will retrieve the current action's name, and map it to its respective
	 * response segment.
	 * Note that it will only change response segments for a predefined set of
	 * action names, all others will default to content.
	 */
	public function preDispatch()
	{
		$request = $this->getRequest();
		switch($request->getActionName())
		{
			case 'logo':
				// Break intentionally omitted
			case 'nav':
				// Break intentionally omitted
			case 'quickview':
				// Break intentionally omitted
			case 'menu':
				// Break intentionally omitted
			case 'notifications':
				// Break intentionally omitted
			case 'sitecontext':
				// Break intentionally omitted
			case 'pagecontext':
				// Break intentionally omitted
			case 'aside':
				// Break intentionally omitted
			case 'links':
				// Break intentionally omitted
			case 'meta':
				$this->_setSegment($request->getActionName());
				break;
			case 'main':
				// Break intentionally omitted
			default:
				$this->_setSegment(null);
		}
	}

	/**
	 * Change the response segment
	 *
	 * @param string $segment The name of the segment to change to
	 */
	protected function _setSegment($segment)
	{
		$viewRenderer = Zend_Controller_Action_HelperBroker
		                	::getStaticHelper('viewRenderer');
		$viewRenderer->setResponseSegment($segment);
	}
}