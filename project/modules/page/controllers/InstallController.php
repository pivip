<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Page
 * @subpackage Controllers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Install the Page module
 */
class InstallController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Install the Page module
	 *
	 * Create the "Blocks" table, add the block to add blocks to a page to the
	 * 'default' bundle and add the 'core' bundle to the 'default' bundle.
	 */
	public function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		if(!Install_Module::isInstalled())
		{
			return;
		}
		$translate = Zend_Registry::get('Zend_Translate');
		try
		{
			Doctrine::generateModelsFromYaml('modules/page/Tables.yml',
			                                 CODE_PATH . 'application/models');
			Doctrine::createTablesFromModels(CODE_PATH . 'application/models');
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			return;
		}
		try
		{
			$block = new Block();
			$block->location = 'default';
			$block->priority = 7;
			$block->action = 'main';
			$block->controller = 'index';
			$block->module = 'page';
			$block->save();
			$bundle = new Block();
			$bundle->location = 'default';
			$bundle->action = 'core';
			$bundle->controller = 'bundle';
			$bundle->module = 'page';
			$bundle->save();
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$error = $translate->_(
				'Could not insert the default blocks into the database.');
			$logger->err($error);
			return;
		}
		$this->_flashMessenger->addMessage($translate->_(
			'The Page module was successfully installed.'));
		$this->_redirect('');
	}
}