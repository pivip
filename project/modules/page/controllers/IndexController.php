<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Page
 * @subpackage Controllers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Load a page's blocks
 */
class IndexController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Load the blocks common to all pages
	 *
	 * Note: setting the 'bundle' request parameter will load a certain bundle,
	 * default bundle is 'default'.
	 */
	public function commonAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$bundle = $this->_request->getParam('bundle');
		if(empty($bundle))
		{
			$bundle = 'default';
		}
		$cache = Page_Module::loadCache();		
		if(!$rows = $cache->load($bundle))
		{
			$query = new Doctrine_Query();
			$query->from('Block b')
			      ->where('b.location=?', $bundle)
			      ->orderby('b.priority ASC');
			$rows = $query->execute();
			$cache->save($rows, $bundle, array('page', 'bundle', 'block')); 
		}
		foreach($rows as $row)
		{
			$params = array('block_id' => $row->block_id);
			$nextRequest = new Zend_Controller_Request_Simple($row->action,
			                                                  $row->controller,
			                                                  $row->module,
			                                                  $params);
			$this->_helper->actionStack($nextRequest);
		}
	}

	/**
	 * Adds stylesheets and loads the blocks of a certain page
	 *
	 * @throws Page_Exception
	 */
	public function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender();

		$contentType = 'text/html; charset=UTF-8';
		$translate = Zend_Registry::get('Zend_Translate');
		$this->view->headMeta()->prependHttpEquiv('Content-Type',
		                                          $contentType);
		$this->view->headMeta()->appendHttpEquiv('Content-Language',
		                                         $translate->getLocale());
		header('Content-Type: ' . $contentType);

		$pageConfig = new Zend_Config_Ini('./modules/page/config.ini');
		try
		{
			$directories = new DirectoryIterator('public/styles');
		} catch(Exception $e) {
			// Fail silently, no stylesheets found
		}
		foreach($directories as $directory)
		{
			if(!$directory->isDir() || $directory->isDot())
			{
				continue;
			}
			$files = new DirectoryIterator($directory->getPathname());
			foreach($files as $file)
			{
				if($file->isDir() || $file->isDot())
				{
					continue;
				}
				if($pageConfig->styles->default == $directory->getFilename())
				{
					$stylesheet = array('href' => $file->getPathname(),
					                    'type' => 'text/css',
					                    'title' => $directory->getFilename(),
					                    'rel' => 'stylesheet',
					                    'media' => str_replace('.css', '',
					                                           $file->getFilename())
					              );
				} else {
					$stylesheet = array('href' => $file->getPathname(),
					                    'type' => 'text/css',
					                    'title' => $directory->getFilename(),
					                    'rel' => 'alternate stylesheet');
				}
				$stylesheet['href'] = $this->_request->getBaseUrl() . '/' .
				                      $stylesheet['href'];
				$this->view->headLink($stylesheet);
			}
		}
		$location = $this->_getLocation();
		$cache = Page_Module::loadCache();
		$cacheId = Page_Module::urlToCacheId($location);
		if(!$rows = $cache->load($cacheId))
		{
			// Check if Page is installed, if not, we have nothing more to do
			if(!Page_Module::isInstalled())
			{
				return;
			}
			try
			{
				$query = new Doctrine_Query();
				$query->from('Block b')
				      ->where('b.location=?', $location)
				      ->orderby('b.priority ASC');
				$rows = $query->execute();
				$cache->save($rows, $cacheId, array('page', 'location', 'block'));
			} catch(Exception $e) {
				$logger = Zend_Registry::get('logger');
				$translate = Zend_Registry::get('Zend_Translate');
				$error = $translate->_('Could not fetch blocks from the database.');
				$logger->err($error);
				// Could not fetch the blocks, don't do anything
				return;
			} 
		}
		foreach($rows as $row)
		{
			$params = array('block_id' => $row->block_id);
			$nextRequest = new Zend_Controller_Request_Simple($row->action,
			                                                  $row->controller,
			                                                  $row->module,
			                                                  $params);
			$this->_helper->actionStack($nextRequest);
		}
		$this->commonAction();
	}

	/**
	 * Display a list of blocks to add
	 */
	public function mainAction()
	{
		$this->_forward('add');
	}

	/**
	 * Add a block to a page
	 */
	public function addAction()
	{
		$location = $this->_request->getParam('location');

		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		if(!$acl->isAllowed('guest', 'block', 'add') && !$auth->hasIdentity())
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$identity = $auth->getIdentityProperties();
		if(!$acl->isAllowed($identity->aclRole, 'block', 'add'))
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}

		$modules = new Pivip_Modules();
		$links = array();
		foreach($modules as $module)
		{
			if(!method_exists($module, 'getPageBlocks'))
			{
				continue;
			}
			$links = array_merge_recursive($links, $module->getPageBlocks($location));
		}
		$this->view->links = $links;
		$this->view->addScriptPath('modules/page/views/scripts/');
	}

	/**
	 * Retrieve the path to the current page
	 *
	 * @return string The current location
	 */
	protected function _getLocation()
	{
		$location = substr($this->_request->getRequestUri(),
		                   strlen($this->_request->getBaseUrl()));
		return $location;
	}
}