<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Page
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Page module
 *
 * The Page module handles detemining which modules need to be loaded for each
 * page.
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Page_Module extends Pivip_Module_Abstract
{
	/**
	 * Defines the dependencies
	 *
	 * Page depends on Pivip
	 */
	protected $_dependencies = array('Pivip' => array('max' => '0.0.0dev'));

	/**
	 * Checks whether this module is already installed
	 *
	 * In the case of the Page module, "installed" means that the "Blocks" table
	 * exists.
	 *
	 * @return boolean Whether Page is already installed.
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseBlock.php'))
		{
			return false;
		}
		try
		{
			$blockTable = Doctrine::getTable('Block');
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * Communicate that Page does not need any configuration
	 *
	 * Always returns false since there is nothing to configure for the Page
	 * module.
	 *
	 * @return boolean False, since Page does not need to be configured.
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Creates the table in the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'page');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Remove the table from the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * Load the generic cache for use by this module
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache()
	{
		$frontendOptions = array('cache_id_prefix' => 'page_',
		                         'automatic_serialization' => true);
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$cache = Zend_Cache::factory('Core',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $cacheConfig->backendOptions->toArray());
		return $cache;
	}

	/**
	 * Normalize a URL so it can be used as a cache ID
	 *
	 * @param  string $url URL to be cache ID-ized
	 * @return string Cache ID-ized URL
	 * @todo   Remove this method
	 */
	public static function urlToCacheId($url)
	{
		return Pivip_Utility_Cache::toCacheId($url);
	}

	/**
	 * Load the module
	 *
	 * First loads the SegmentMapper Action Helper and makes sure Page's index
	 * controller is called by default. It then sets the layout which will
	 * include all blocks. Of course, all related routes and view helpers are
	 * also loaded. 
	 * It also saves a "default request" to the registry. Other modules can
	 * forward to this request when they have been called directly and thus the
	 * rest of the page has not been loaded.
	 */
	public function bootstrap()
	{
		// Make sure that other modules can extends Page_Abstract
		require 'modules/page/Abstract.php';
		/**
		 * Load the SegmentMapper action helper
		 *
		 * @see modules/page/helpers/SegmentMapper.php
		 */
		$frontController = Zend_Controller_Front::getInstance();
		require_once 'helpers/SegmentMapper.php';
		Zend_Controller_Action_HelperBroker::addHelper(new SegmentMapper());
		// Make sure this module is loaded when no other module is
		$frontController->setDefaultModule('page');
		// Initialize the layout
		Zend_Layout::startMvc(array('layoutPath' =>
		                            	'modules/page/views/scripts'));
		// Set the website title
		$generalConfig = Zend_Registry::get('generalConfig');
		$viewRenderer = Zend_Controller_Action_HelperBroker
		                	::getStaticHelper('viewRenderer');
		$viewRenderer->view->headTitle($generalConfig->website->name);
		// Add the view helpers
		$viewRenderer->view->addHelperPath('modules/page/views/helpers',
		                                   'Page_View_Helper');
		// Define the Access Control List
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('block'))
		    ->allow('admin', 'block', array('add', 'edit', 'delete'));
		Zend_Registry::set('acl', $acl);
		// Make sure other modules can forward to the page controller
		$defaultRequest = new Zend_Controller_Request_Simple('index',
		                                                     'index',
		                                                     'page',
		                                                     array('bundle'
		                                                           => 'core'));
		Zend_Registry::set('defaultRequest', $defaultRequest);
		$auth = Pivip_Auth::getInstance();
		if(!$this->isInstalled() && $auth->hasIdentity())
		{
			$properties = $auth->getIdentityProperties();
			if($acl->isAllowed((string) $properties->aclRole, 'module', 'install'))
			{
				$this->install();
			}
		}
	}
}