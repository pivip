<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Friends
 * @subpackage Models
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Contact list model
 */
class ContactList
{
	/**
	 * Add a contact
	 *
	 * @param string $id Contact identifier
	 */
	public function add($website, $name)
	{
		$website = rtrim($website, '/') . '/';
		$friendsTable = Doctrine::getTable('Friend');
		if(null !== $friendsTable->find($website))
		{
			return array('status' => false,
			             'msg' => $translate->_('You have already been added.'));
		}
		$friend = new Friend();
		$friend->website = $website;
		$friend->name = $name;
		$translate = Zend_Registry::get('Zend_Translate');
		try
		{
			$friend->save();
			$cache = Friends_Module::loadCache();
			$cache->remove('friendlist');
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->notice($e->getMessage());
			return array('status' => false,
			             'msg' => $translate->_('Could not save the request.'));
		}
		return array('status' => true,
		             'msg' => $translate->_('Request saved.'));
	}
}