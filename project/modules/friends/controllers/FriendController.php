<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Friends
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage friends
 */
class Friends_FriendController extends Page_Abstract
{
	/**
	 * Add a contact
	 *
	 * If the user is not allowed to approve contacts, just apply and redirect
	 * to the homepage.
	 */
	public function addAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		if(!$this->_isAllowed('apply'))
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'You are not allowed to apply for the contact list.'));
			$this->_redirect('/');
		}
		$auth = Pivip_Auth::getInstance();
		if(!$this->_isAllowed('approve'))
		{
			try
			{
				$friend = new Friend();
				$identity = $auth->getIdentityProperties();
				$friend->website = $identity->url;
				$friend->name = $identity->fn;
				$friend->save();
			} catch(Exception $e) {
				$this->_flashMessenger->setNamespace('error')
				->addMessage($translate->_(
					'There was an error applying for the contact list.'));
				$this->_redirect('/');
			}
			$this->_flashMessenger->addMessage($translate->_(
'Your request to be added to the contact list is pending approval.'));
			$this->_redirect('/');
		}
		$form = $this->_getForm();
		$website = urldecode($this->_request->getParam('friend'));
		$friend = null;
		if(!empty($website))
		{
			$friendsTable = Doctrine::getTable('Friend');
			$friend = $friendsTable->find($website);
			if(null !== $friend)
			{
				$data = array('name' => $friend->name, 'website' => $friend->website);
				$form->populate($data);
			}
		}
		$this->view->form = $form->render();
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$this->_processForm($form, $friend);
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_refresh('The contact could not be added, please try again.');
		}
		$this->_flashMessenger->addMessage($translate->_(
'The contact has been added to the contact list.'));
		if($this->_sendFriendRequest($friend->website))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'A friend request has been sent.'));
		}
		$this->_redirect('/');
	}

	/**
	 * Edit a contact
	 */
	public function editAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		if(!$this->_isAllowed('approve'))
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'You are not allowed to edit contacts.'));
			$this->_redirect('/');
		}
		$website = urldecode($this->_request->getParam('friend'));
		if(null === $website)
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'Please specify a contact to edit.'));
			$this->_redirect('/');
		}
		$form = $this->_getForm();
		$friendsTable = Doctrine::getTable('Friend');
		$friend = $friendsTable->find($website);
		if(null === $friend)
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'This contact could not be found'));
			$this->_redirect('/');
		}
		$data = array();
		foreach($friend as $k => $v)
		{
			$data[$k] = $v;
		}
		$form->populate($data);
		$form->submit->setLabel('Edit contact');
		$this->view->form = $form;
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$this->_processForm($form, $friend);
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_refresh('The contact could not be edited, please try again.');
		}
		$this->_flashMessenger->addMessage($translate->_(
'The contact has been added to the contact list.'));
		$this->_redirect('/');
	}

	/**
	 * Delete a contact
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$friend = $this->_request->getParam('friend');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $friend)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No contact specified.'));
			$this->_redirect('/');
		}

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to delete contacts.'));
			$this->_redirect('/');
		}

		$friendsTable = Doctrine::getTable('Friend');
		$friend = $friendsTable->find(urldecode($friend));

		try
		{
			$friend->delete();
			$this->_flashMessenger->addMessage($translate->_(
				'Contact removed.'));
			$cache = Friends_Module::loadCache();
			$cache->remove('friendlist');
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'An error occurred while removing the contact, ' .
				'please try again.'));
		}
		$this->_redirect('/');
	}

	/**
	 * Manage REST requests
	 */
	public function restAction()
	{
		if(!$this->_isAllowed('apply'))
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		require_once 'modules/friends/models/ContactList.php';
		$server = new Zend_Rest_Server();
		$server->setClass('ContactList');
		$server->handle();
	}

	/**
	 * @return Zend_Form The form to add a friend
	 */
	protected function _getForm()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$form = new Zend_Form;
		$form->setMethod('post')
		     ->setAction($this->_request->getRequestUri())
		     ->addElement('text', 'name')
		     ->name->setRequired(true)
		           ->setLabel('Contact name')
		           ->addValidator('NotEmpty');
		$form->addElement('text', 'website')
		     ->website->setRequired(true)
		              ->setLabel('Contact website')
		              ->addValidator('NotEmpty');
		$form->addDisplayGroup(array('name', 'website'), 'contact')
		     ->contact->setLegend('Contact');
		$form->addElement('checkbox', 'met')
		     ->met->setLabel('We have met');
		$form->addElement('select', 'friendship')
		     ->friendship->setLabel('We are')
		                 ->addMultiOption('none', $translate->_('Nothing'))
		                 ->addMultiOption('contact', $translate->_('Contacts'))
		                 ->addMultiOption('acquaintance',
		                                  $translate->_('Acquaintances'))
		                 ->addMultiOption('friend', $translate->_('Friends'))
		                 ->setValue('none');
		$form->addElement('checkbox', 'coworker')
		     ->coworker->setLabel('I work with this person');
		$form->addElement('checkbox', 'colleague')
		     ->colleague->setLabel('This is a colleague');
		$form->addElement('select', 'geographical')
		     ->geographical->setLabel('This person lives')
		                   ->addMultiOption('co-resident',
		                                    $translate->_('In the same street'))
		                   ->addMultiOption('neighbor',
		                                    $translate->_('Nearby'))
		                   ->addMultiOption('none',
		                                    $translate->_('Far away'))
		                   ->setValue('none');
		$form->addElement('select', 'family')
		     ->family->setLabel('This person is a')
		             ->addMultiOption('none', $translate->_('Nothing'))
		             ->addMultiOption('child', $translate->_('Child'))
		             ->addMultiOption('parent', $translate->_('Parent'))
		             ->addMultiOption('sibling',
		                              $translate->_('Brother or sister'))
		             ->addMultiOption('spouse',
		                              $translate->_('Husband or wife'))
		             ->addMultiOption('kin', $translate->_('Relative'))
		             ->setValue('none');
		$form->addElement('checkbox', 'muse')
		     ->muse->setLabel('This person inspires me');
		$form->addElement('checkbox', 'crush')
		     ->crush->setLabel('I have a crush on this person');
		$form->addElement('checkbox', 'date')
		     ->date->setLabel('I date this person');
		$form->addElement('checkbox', 'sweetheart')
		     ->sweetheart->setLabel('We are intimate');
		$form->addElement('checkbox', 'me')
		     ->me->setLabel('This is me');
		$form->addDisplayGroup(array('friendship', 'met', 'coworker', 'colleague',
		                             'geographical', 'family', 'muse', 'crush',
		                             'date', 'sweetheart', 'me'),
		                       'relationship')
		     ->relationship->setLegend('Relationship');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Add contact')
		       ->addDecorator('HtmlTag', array('tag' => 'dd'))
		       ->removeDecorator('DtDdWrapper');
		$form->addElement($submit);
		return $form;
	}

	/**
	 * Process a submitted form
	 *
	 * @param Zend_Form $form
	 */
	protected function _processForm(Zend_Form $form, $friend = null)
	{
		if(empty($friend))
		{
			$friend = new Friend();
		}
		$friend->website = rtrim($form->getValue('website'), '/') . '/';
		$friend->name = $form->getValue('name');
		$friend->met = $form->getValue('met');
		$friend->friendship = $form->getValue('friendship');
		$friend->coworker = $form->getValue('coworker');
		$friend->colleague = $form->getValue('colleague');
		$friend->geographical = $form->getValue('geographical');
		$friend->family = $form->getValue('family');
		$friend->muse = $form->getValue('muse');
		$friend->crush = $form->getValue('crush');
		$friend->date = $form->getValue('date');
		$friend->sweetheart = $form->getValue('sweetheart');
		$friend->me = $form->getValue('me');
		$friend->approved = true;
		$friend->save();
		$cache = Friends_Module::loadCache();
		$cache->remove('friendlist');
	}

	/**
	 * Send a friend request to a website
	 *
	 * @param string $website The website to send the request to
	 */
	protected function _sendFriendRequest($website)
	{
		if(!Zend_Uri::check($friend->website))
		{
			return false;
		}
		$restAddress = rtrim($website, '/') . '/contacts/rest';
		$client = new Zend_Rest_Client($restAddress);
		$generalConfig = Zend_Registry::get('generalConfig');
		$link = Vogel_Utility_Url::absoluteUrl($this->_request->getBaseUrl());
		$result = $client->add($link, $generalConfig->website->name)->get();
		return $result->isSuccess();
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if(!$auth->hasIdentity())
		{
			return $acl->isAllowed('guest', 'friendlist', $privileges);
		}		
		return $acl->isAllowed($identity->aclRole, 'friendlist', $privileges);
	}
}