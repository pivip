<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Friends
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Display a list of contacts
 */
class Friends_ListController extends Page_Abstract
{
	/**
	 * Display the friend lists in the sitecontext section of the website
	 */
	public function sitecontextAction()
	{
		require_once 'modules/friends/views/helpers/GetXfnLink.php';
		$cache = Friends_Module::loadCache();
		if(!$rows = $cache->load('friendlist'))
		{
			$query = Doctrine_Query::create();
			$query->select('*')
			      ->from('Friend')
			      ->where('approved=?', true);
			$rows = $query->execute();
			$cache->save($rows, 'friendlist',
			             array('friends', 'list', 'friendlist'));
		}
		$this->view->contacts = $rows;
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if($auth->hasIdentity() &&
		   $acl->isAllowed($identity->aclRole, 'friendlist', 'apply'))
		{
			$this->view->showOptions = true;
			$this->view->allowApply = true;
		}
		if($this->_isAllowed('edit'))
		{
			$this->view->showOptions = true;
			$this->view->allowEdit = true;
		}
	}

	/**
	 * Add a friend list
	 */
	public function addAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$translate = Zend_Registry::get('Zend_Translate');
		$location = '/' . $this->_request->getParam('location');
		if(!$this->_isAllowed('add'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to add a contact.'));
			$this->_redirect($location);
		}
		$section = $this->_request->getParam('section');
		if(empty($section))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You need to specify a section to add the contact list to.'));
			$this->_redirect($location);
		}
		try
		{
			$block = new Block();
			$block->location = $location;
			$block->action = $section;
			$block->controller = 'list';
			$block->module = 'friends';
			$block->save();
			$cacheId = Pivip_Utility_Cache::toCacheId($location);
			$cache = Page_Module::loadCache();
			$cache->remove($cacheId);
			$this->_flashMessenger
			->addMessage($translate->_('Contact list added.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_('Failed to add the contact list.'));
		}
		$this->_redirect($location);
	}

	/**
	 * Edit the friend list
	 */
	public function editAction()
	{
		if(!$this->_isAllowed('edit'))
		{
			$translate = Zend_Registry::get('Zend_Translate');
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'You do not have enough rights to edit the contact list.'));
			$this->_redirect('/');
		}
		$query = Doctrine_Query::create();
		$query->select('*')
		      ->from('Friend')
		      ->where('approved=?', true);
		$rows = $query->execute();
		$this->view->contacts = $rows;
		$query = Doctrine_Query::create();
		$query->select('name, website')
		      ->from('Friend')
		      ->where('approved=?', false);
		$rows = $query->execute();
		$this->view->toApprove = $rows;
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
	}

	/**
	 * Delete a friend list
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$listId = $this->_request->getParam('list_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $listId)
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'No contact list specified.'));
			$this->_redirect();
		}

		$blockTable = Doctrine::getTable('Block');
		$block = $blockTable->find($listId);

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'You are not allowed to delete the contact list.'));
			$this->_redirect($block->location);
		}

		try
		{
			$block->delete();
			$this->_flashMessenger->addMessage($translate->_(
				'Contact list deleted.'));
			$cache = Page_Module::loadCache();
			$cache->remove(Pivip_Utility_Cache::toCacheId($location));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'An error occurred while deleting the contact list, ' .
				'please try again.'));
		}
		$this->_redirect($location);
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges = null)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if(!$auth->hasIdentity())
		{
			return $acl->isAllowed('guest', 'block', $privileges);
		}
		return $acl->isAllowed($identity->aclRole, 'block', $privileges);
	}
}