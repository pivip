<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Navigation
 * @subpackage ViewHelpers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Convert a contact's data to an XFN link
 *
 * @link http://www.gmpg.org/xfn/
 */
class Friends_View_Helper_GetXfnLink
{
	public $view;

	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 * Convert a contact's data to an XFN link
	 *
	 * @link   http://www.gmpg.org/xfn/
	 * @param  Doctrine_Collection $contact The contact
	 * @return string The XFN link
	 */
	public function getXfnLink($contact)
	{
		$rel = '';
		if($contact->met)
		{
			$rel .= 'met';
		}
		switch($contact->friendship)
		{
			case 'contact':
			case 'acquaintance':
			case 'friend':
				$rel .= ' ' . $contact->friendship;
				break;
		}
		if($contact->coworker)
		{
			$rel .= ' coworker';
		}
		if($contact->colleague)
		{
			$rel .= ' colleague';
		}
		switch($contact->geographical)
		{
			case 'co-resident':
			case 'neighbor':
				$rel .= ' ' . $contact->geographical;
				break;
		}
		switch($contact->family)
		{
			case 'child':
			case 'parent':
			case 'sibling':
			case 'spouse':
			case 'kin':
				$rel .= ' ' . $contact->family;
				break;
		}
		if($contact->muse)
		{
			$rel .= ' muse';
		}
		if($contact->crush)
		{
			$rel .= ' crush';
		}
		if($contact->date)
		{
			$rel .= ' date';
		}
		if($contact->sweetheart)
		{
			$rel .= ' sweetheart';
		}
		if($contact->me)
		{
			$rel .= ' me';
		}
		$link = '
<a href="' . $contact->website . '"
   rel="' . $rel . '"
   title="' . $this->view->translate('Visit contact\'s website') . '"
>' . $contact->name . '</a>';
		return $link;		
	}
}