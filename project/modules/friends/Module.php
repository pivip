<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Friends
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Friends module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Friends_Module extends Pivip_Module_Abstract
{
	/**
	 * Checks whether the Friends module is installed
	 *
	 * The Friends module is installed if the database tables have been created.
	 *
	 * @return boolean Whether Friends is already installed.
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH
		                . 'application/models/generated/BaseFriend.php'))
		{
			return false;
		}
		try
		{
			$friendsTable = Doctrine::getTable('Friend');
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * @return boolean False
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Creates the table in the database and perform the first authentication
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
		if($this->isInstalled())
		{
			return;
		}
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'friends');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Remove the table from the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * @return array A list of the blocks this module provides for normal pages,
	 *               in the format:
	 *               array('section' => array('Link to add this block'
	                                          	=> 'Block name'))
	 */
	public static function getPageBlocks($location)
	{
		if(!self::isInstalled())
		{
			return array();
		}
		$translate = Zend_Registry::get('Zend_Translate');
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Friends_AddList');
		$blocks = array();
		$sections = array('sitecontext');
		foreach($sections as $section)
		{
			$blocks[$section] = array($route->assemble(array('section' => $section,
			                                                 'location'
			                                                 	=> $location))
			                          	=> $translate->_('Contact list'));
		}
		return $blocks;
	}

	/**
	 * Load the generic cache for use by this module
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache()
	{
		$frontendOptions = array('automatic_serialization' => true);
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$cache = Zend_Cache::factory('Core',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $cacheConfig->backendOptions->toArray());
		return $cache;
	}

	/**
	 * Load the module
	 *
	 * @todo Allow the admins to edit the ACL.
	 */
	public function bootstrap()
	{
		// Only load if installed
		if(!$this->isInstalled())
		{
			return;
		}
		// Define the Access Control List
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('friendlist'))
		    ->allow('guest', 'friendlist', 'apply');
		Zend_Registry::set('acl', $acl);
		// Define the routes
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$options = array('module' => 'friends', 'controller' => 'list',
		                 'action' => 'add', 'location' => '');
		$route = new Zend_Controller_Router_Route(
		         	'contactlist/add/:section/:location', $options);
		$router->addRoute('Friends_AddList', $route);
		$options = array('module' => 'friends', 'controller' => 'list',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route_Static(
		         	'contactlist/edit', $options);
		$router->addRoute('Friends_EditList', $route);
		$options = array('module' => 'friends', 'controller' => 'friend',
		                 'action' => 'add');
		$route = new Zend_Controller_Router_Route('contacts/apply',
		                                          $options);
		$router->addRoute('Friends_ApplyAsFriend', $route);
		$options = array('module' => 'friends', 'controller' => 'friend',
		                 'action' => 'add', 'friend' => '');
		$route = new Zend_Controller_Router_Route('contacts/add/:friend',
		                                          $options);
		$router->addRoute('Friends_AddFriend', $route);
		$options = array('module' => 'friends', 'controller' => 'friend',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route('contacts/edit/:friend',
		                                          $options);
		$router->addRoute('Friends_EditFriend', $route);
		$options = array('module' => 'friends', 'controller' => 'friend',
		                 'action' => 'delete');
		$route = new Zend_Controller_Router_Route('contacts/delete/:friend',
		                                          $options);
		$router->addRoute('Friends_DeleteFriend', $route);
		$options = array('module' => 'friends', 'controller' => 'friend',
		                 'action' => 'rest');
		$route = new Zend_Controller_Router_Route('contacts/rest/*', $options);
		$router->addRoute('Friends_Rest', $route);
	}
}