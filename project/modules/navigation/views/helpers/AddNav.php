<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Navigation
 * @subpackage ViewHelpers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Add an item to the navigation menu
 */
class Navigation_View_Helper_AddNav
{
	public $view;

	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 * Add an item to the navigation menu
	 *
	 * @param string $target The link target
	 * @param string $label  Label of the link
	 * @param array  $params Additional attributes
	 */
	public function addNav($target, $label, $params = null)
	{
		if(null === $params || !is_array($params))
		{
			$params = ' title=' . $label;
		} else {
			if(empty($params['title']))
			{
				$params['title'] = $label;
			}
			$string = '';
			foreach($params as $attr => $value)
			{
				$string .= ' ' . $attr . '="' . $value . '"';
			}
			$params = $string;
		}
		$this->view->placeholder('nav')->append(
'<a href="' . $target . '"' . $params . '>' . $label . '</a>');
	}
}