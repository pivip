<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Navigation
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Navigation module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Navigation_Module extends Pivip_Module_Abstract
{
	/**
	 * Check if the Block table exists and if the nav block has been added to it.
	 *
	 * @return boolean Whether the Navigation module is installed
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseBlock.php'))
		{
			return false;
		}
		$cache = Page_Module::loadCache();		
		if(!$rows = $cache->load('core'))
		{
			try
			{
				$query = new Doctrine_Query();
				$query->from('Block b')
				      ->where('b.location=?', 'core')
				      ->orderby('b.priority ASC');
				$rows = $query->execute();
			} catch(Exception $e) {
				return false;
			}
		}
		foreach($rows as $row)
		{
			if('navigation' == $row->module &&
			   'index' == $row->controller &&
			   'nav' == $row->action &&
			   'core' == $row->location)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Communicate that Navigation accepts configuration
	 *
	 * @return boolean True
	 */
	public static function needsConfiguring()
	{
		return true;
	}

	/**
	 * Install the Navigation module
	 */
	public function install()
	{
		if($this->isInstalled())
		{
			return;
		}
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'navigation');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Add the Navigation's view helpers
	 */
	public function bootstrap()
	{
		$viewRenderer =
			Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
		$viewRenderer->view->addHelperPath('modules/navigation/views/helpers',
		                                   'Navigation_View_Helper');
		if(!$this->isInstalled() && Page_Module::isInstalled())
		{
			$this->install();
		}
	}
}