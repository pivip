<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Branding
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Branding module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Branding_Module extends Pivip_Module_Abstract
{
	/**
	 * Check whether the branding block has been added to the header
	 *
	 * @return boolean Whether the Branding module is installed 
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseBlock.php'))
		{
			return false;
		}
		if(!Page_Module::isInstalled())
		{
			return true;
		}
		$cache = Page_Module::loadCache();		
		if(!$rows = $cache->load('core'))
		{
			try
			{
				$query = new Doctrine_Query();
				$query->from('Block b')
				      ->where('b.location=?', 'core')
				      ->orderby('b.priority ASC');
				$rows = $query->execute();
			} catch(Exception $e) {
				return false;
			}
		}
		foreach($rows as $row)
		{
			if('branding' == $row->module &&
			   'branding' == $row->controller &&
			   'logo' == $row->action &&
			   'core' == $row->location)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Communicate that Branding accepts configuration
	 *
	 * @return boolean True
	 */
	public static function needsConfiguring()
	{
		return true;
	}

	/**
	 * Add the module to the "core" bundle
	 */
	public function install()
	{
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'branding');
		$this->_pushStack($nextRequest);
	}
}