<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Branding
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Allow for branding of the website
 */
class Branding_BrandingController extends Pivip_Controller_Module_Abstract
{
	/**
	 * @var Zend_Config_Ini The Branding configuration
	 */
	protected static $_config;

	/**
	 * Load the Branding configuration
	 */
	protected static function _loadConfig()
	{
		if(empty(self::$_config))
		{
			self::$_config = new Zend_Config_Ini('modules/branding/config.ini');
		}
		return self::$_config;
	}

	/**
	 * Display the logo in the header
	 */
	public function logoAction()
	{
		$generalConfig = Zend_Registry::get('generalConfig');
		$this->view->websiteName = $generalConfig->website->name;
		$brandingConfig = self::_loadConfig();
		if(!empty($brandingConfig->header->image))
		{
			$this->view->headerImage = $brandingConfig->header->image;
		}
	}
}