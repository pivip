<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Openid
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Openid module
 *
 * The Openid module allows for authentication using OpenID.
 *
 * @see /library/Pivip/Module/Abstract.php
 * @link http://openid.net/ OpenID.net
 */
class Openid_Module extends Pivip_Module_Abstract
{
	/**
	 * @var Vogel_Config_Ini The OpenID configuration
	 */
	protected static $_config;

	/**
	 * Load the OpenID configuration
	 */
	protected static function _loadConfig()
	{
		if(empty(self::$_config))
		{
			self::$_config = new Vogel_Config_Ini('./modules/openid/config.ini');
		}
		return self::$_config;
	}

	/**
	 * Defines the dependencies
	 *
	 * Openid depends on Pivip and the Page module
	 */
	public function __construct()
	{
	}

	/**
	 * Checks whether the OpenID database table exists
	 *
	 * Will check whether a superuser is defined and whether the database tables
	 * exist.
	 *
	 * @return boolean Whether Openid is already installed.
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH
		                . 'application/models/generated/BaseOpenid.php'))
		{
			return false;
		}
		$config = self::_loadConfig();
		if(empty($config->administration->superuser))
		{
			return false;
		}
		return true;
	}

	/**
	 * Check whether the authentication form has been added to the pages
	 *
	 * @return boolean Whether the form has been added.
	 */
	public static function blockAdded()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseBlock.php'))
		{
			return false;
		}
		$cache = Page_Module::loadCache();		
		if(!$rows = $cache->load('core'))
		{
			try
			{
				$query = new Doctrine_Query();
				$query->from('Block b')
				      ->where('b.location=?', 'core')
				      ->orderby('b.priority ASC');
				$rows = $query->execute();
			} catch(Exception $e) {
				return false;
			}
		}
		foreach($rows as $row)
		{
			if('openid' == $row->module &&
			   'authentication' == $row->controller &&
			   'menu' == $row->action &&
			   'core' == $row->location)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Communicate that Openid needs configuration if true
	 *
	 * If the superuser has not been defined, returns true.
	 *
	 * @return boolean True if OpenID still needs to be configured
	 */
	public static function needsConfiguring()
	{
		$config = self::_loadConfig();
		if(empty($config->administration->superuser))
		{
			return true;
		}
		return false;
	}

	/**
	 * Creates the table in the database and perform the first authentication
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
		if($this->isInstalled())
		{
			return;
		}
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'openid');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Remove the table from the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * Convert OpenID's to cache ID's
	 *
	 * @param  string $openid OpenID
	 * @return string Cache id 
	 */
	public static function openidToCacheId($openid)
	{
		return 'openid_id_' .  Pivip_Utility_Cache::toCacheId($openid);
	}

	/**
	 * Change details retreived using Sreg to the Identity Properties format
	 *
	 * @param  array $details Details retrieved using Sreg
	 * @return array Array containing the Identity Properties
	 */
	public static function sregToProperties(array $details)
	{
		if(empty($details['nickname']) || empty($details['id']))
		{
			throw new InvalidArgumentException();
		}
		$data = array('nickname' => $details['nickname'],
		              'uid' => $details['id']);
		if(!empty($details['fullname']))
		{
			$data['fn'] = $details['fullname']; 
		} else {
			$data['fn'] = $details['nickname'];
		}
		if(!empty($details['email']))
		{
			$data['email'] = $details['email'];
		}
		if(!empty($details['dob']) && 10 == strlen($details['dob']))
		{
			$data['bday'] = $details['dob'];
		}
		if(!empty($details['gender']))
		{
			$data['sex'] = strtolower($details['gender']);
		}
		if(!empty($details['postcode']))
		{
			$data['adr'] = ';;;;;' . $details['postcode'];
		} else {
			$data['adr'] = ';;;;;';
		}
		if(!empty($details['country']))
		{
			$locale = new Zend_Locale();
			$data['adr'] .= ';' . $locale->getCountryTranslation(
			                      $details['country']);
		} else {
			$data['adr'] .= ';';
		}
		if(!empty($details['language']))
		{
			$data['locale'] = $details['language'];
			if(!empty($details['country']))
			{
				$data['locale'] .= '_' . $details['country'];
			}
		}
		if(!empty($details['timezone']))
		{
			$data['tz'] = 'VALUE=text:' . $details['timezone'];
		}
		return $data;
	}

	/**
	 * Load the OpenID cache
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache()
	{
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$options = $cacheConfig->toArray();
		$separator = '';
		if('/' != substr($options['cache']['cache_root'], -1))
		{
			$separator = '/';
		}
		$options['backendOptions']['cache_dir'] = $options['cache']['cache_root']
		                                           . $separator
		                                           . 'modules/openid';
		$frontendOptions = array('cache_id_prefix' => 'openid_',
		                         'automatic_serialization' => true);
		$cache = Zend_Cache::factory('Page',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $options['backendOptions']);
		return $cache;
	}

	/**
	 * Load the module
	 *
	 * Register the OpenID Authentication adapter with Zend_Auth, process a login
	 * form if submitted and, if no superuser has been set yet, prompt the user
	 * to assign one.
	 *
	 * @todo Allow the admins to edit the ACL.
	 * @todo Do not add a block unasked if the Page module is installed
	 * @todo If the Page module has not yet been installed, load the login form
	 *       in the main section instead of the menu.
	 */
	public function bootstrap()
	{
		// If Pivip has been installed but this module has not,
		// commence installation so we can authenticate and authorize users
		if(!$this->isInstalled())
		{
			if(Install_Module::isInstalled())
			{
				$this->install();
			}
			return;
		}
		// If the Page module has been installed, make sure that a login
		// form is added
		if(!$this->blockAdded() && Page_Module::isInstalled())
		{
			$nextRequest = new Zend_Controller_Request_Simple('block',
			                                                  'install',
			                                                  'openid');
			self::_pushStack($nextRequest);
		}
		// If the Page module has not yet been installed, make sure the visitor
		// can login
		if(!Page_Module::isInstalled())
		{
			$nextRequest = new Zend_Controller_Request_Simple('menu',
			                                                  'authentication',
			                                                  'openid');
			self::_pushStack($nextRequest);
		}
		$request = new Zend_Controller_Request_Http();
		$openid_identifier = $request->getPost('openid_identifier');
		$openid_action = $request->getPost('openid_action');
		$openid_mode = $request->getQuery('openid_mode');
		if(($request->isPost() &&
		    !empty($openid_identifier) &&
		    !empty($openid_action)) ||
		   !empty($openid_mode))
		{
			$nextRequest = new Zend_Controller_Request_Simple('process',
			                                                  'authentication',
			                                                  'openid');
			self::_pushStack($nextRequest);
		}

		$router = Zend_Controller_Front::getInstance()->getRouter();
		$options = array('module' => 'openid', 'controller' => 'authentication',
		                 'action' => 'logout');
		$route = new Zend_Controller_Router_Route_Static('account/logout',
		                                                 $options);
		$router->addRoute('openidLogout', $route);
	}
}