<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Openid
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Install the OpenID module
 */
class Openid_InstallController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Will hold the configuration
	 */
	protected static $_config;

	/**
	 * Install the OpenID module
	 */
	public function indexAction()
	{
		$requestParams = Zend_Controller_Front::getInstance()->getRequest()
		                 ->getParams();
		if(Openid_Module::isInstalled() ||
		   ('configuration' == $requestParams['action'] &&
		    'install' == $requestParams['controller'] &&
		    'openid' == $requestParams['module']))
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		if(file_exists(CODE_PATH
		                . 'application/models/generated/BaseOpenid.php'))
		{
			$this->_forward('configuration');
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$translate = Zend_Registry::get('Zend_Translate');
		try
		{
			Doctrine::generateModelsFromYaml('modules/openid/Tables.yml',
			                                 CODE_PATH . 'application/models');
			Doctrine::createTablesFromModels(CODE_PATH . 'application/models');
		} catch(Exception $e) {
			$this->_flashMessenger->addMessage($translate->_(
				'There was an error generating the OpenID table models,
				make sure scripts have write permission in the models folder.'));
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_helper->redirector->goto('configuration');
		}
		$this->_forward('configuration');
	}

	/**
	 * Configure the OpenID module
	 *
	 * @todo Display error messages instead of silently failing
	 */
	public function configurationAction()
	{
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack->pushStack($defaultRequest);
		$translate = Zend_Registry::get('Zend_Translate');
		$logger = Zend_Registry::get('logger');
		$config = self::_loadConfig();
		$superuser = new Zend_Form_Element_Text('superuser');
		$superuser->setLabel('Administration OpenID')
		          ->setRequired(true);
		$form = new Zend_Form();
		$form->setAction($this->_request->getRequestUri())
		     ->setMethod('post')
		     ->setAttrib('id', 'configureOpenid')
		     ->setAttrib('class', 'install configure openid')
		     ->addElement($superuser);
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Configure');
		$form->addElement($submit);
		$data['superuser'] = $config->administration->superuser;
		$form->populate($data);
		$this->view->form = $form;
		$requirements = array('nickname' => true, 'email' => false,
		                      'fullname' => false, 'dob' => false,
		                      'gender' => false, 'postcode' => false,
		                      'country' => false, 'language' => false,
		                      'timezone' => false);
		$sreg = new Zend_OpenId_Extension_Sreg($requirements, null, 1.1);
		if($this->_request->isPost() &&
		   !$form->isValid($this->_request->getPost()))
		{
			$this->_refresh('Please fill in all fields.', 'error');
		} else if($this->_request->isPost()) {
			$id = $this->_request->getParam('superuser');
			$consumer = new Zend_OpenId_Consumer();
			if(!$consumer->login($id, null, null, $sreg))
			{
				$error = sprintf($translate->_('OpenID %s is invalid.'), $id);
				$logger->err($error);
				$this->_flashMessenger->setNamespace('error')->addMessage($error);
			}
		}
		if('id_res' == $this->_request->getParam('openid_mode'))
		{
			$consumer = new Zend_OpenId_Consumer();
			if($consumer->verify($this->_request->getParams(), $id, $sreg))
			{
				$config->administration->superuser = $id;
				$details = $sreg->getProperties();
				$details['id'] = $id;
				if(!empty($details['nickname']))
				{
					$data = Openid_Module::sregToProperties($details);
					$data['acl_role'] = 'superuser';
					try
					{
						$openidTable = Doctrine::getTable('Openid');
						$openid = $openidTable->find($id);
						if(false === $openid)
						{
							$openid = new Openid();
						}
						$openid->merge($data);
						$openid->save();
						$cache = Openid_Module::loadCache();
						$cache->remove(Openid_Module::openidToCacheId($id));
					} catch(Exception $e) {
						$error = $translate->_(
							'Could not save OpenID data to the database.' );
						$logger->err($e->getMessage());
						$this->_flashMessenger->setNamespace('error')->addMessage($error);
					}
				}
				try
				{
					$config->save();
					$success = $translate->_('OpenID set.');
					$this->_flashMessenger->resetNamespace()->addMessage($success);
					$this->_redirect('');
				} catch(Exception $e) {
					$error = $translate->_(
						'Could not save the OpenID configuration to file.');
					$logger->err($e->getMessage());
					$this->_flashMessenger->setNamespace('error')->addMessage($error);
				}
			} else {
				$error = sprintf($translate->_('Could not authenticate OpenID %s.'),
				                 $id);
				$logger->err($error);
				$this->_flashMessenger->setNamespace('error')->addMessage($error);
			}
		}
	}

	/**
	 * Add the login form to the pages
	 */
	public function blockAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		if(Openid_Module::blockAdded() || !Page_Module::isInstalled())
		{
			return;
		}
		try
		{
			$block = new Block();
			$block->location = 'core';
			$block->action = 'menu';
			$block->controller = 'authentication';
			$block->module = 'openid';
			$block->save();
			$cache = Page_Module::loadCache();
			$cache->remove('core');
		} catch(Exception $e) {
			// Could not add the block, too bad, load the rest of the page
		}
	}

	/**
	 * Load the OpenID configuration
	 */
	protected static function _loadConfig()
	{
		if(empty(self::$_config))
		{
			self::$_config = new Vogel_Config_Ini('./modules/openid/config.ini');
		}
		return self::$_config;
	}
}