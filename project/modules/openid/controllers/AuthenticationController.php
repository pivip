<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Openid
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Display authentication forms
 */
class Openid_AuthenticationController extends Page_Abstract
{
	/**
	 * Generate a login form
	 */
	protected function _getForm()
	{
		$action =
			Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
		$openid_identifier = new Zend_Form_Element_Text('openid_identifier');
		$openid_identifier->setRequired(true)
		                  ->setLabel('OpenID');
		$submit = new Zend_Form_Element_Submit('openid_action');
		$submit->setLabel('Sign in')
		       ->setValue('login');
		$form = new Zend_Form();
		$form->setAction($action)
		     ->setMethod('post')
		     ->setAttrib('class', 'openid')
		     ->addElement($openid_identifier)
		     ->addElement($submit);
		return $form;
	}

	/**
	 * Process a login
	 *
	 * @todo Allow registration of new OpenIDs when 1 != count($rows)
	 */
	public function processAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$auth = Pivip_Auth::getInstance();
		$translate = Zend_Registry::get('Zend_Translate');
		$openid_identifier = $this->_request->getPost('openid_identifier');
		$openid_action = $this->_request->getPost('openid_action');
		$openid_mode = $this->_request->getQuery('openid_mode');

		$requirements = array('nickname' => true, 'email' => false,
		                      'fullname' => false, 'dob' => false,
		                      'gender' => false, 'postcode' => false,
		                      'country' => false, 'language' => false,
		                      'timezone' => false);
		$sreg = new Zend_OpenId_Extension_Sreg($requirements, null, 1.1);

		if($this->_request->isPost() &&
		   !empty($openid_identifier) &&
		   !empty($openid_action))
		{
			$normalizedId = $openid_identifier;
			Zend_OpenId::normalize($normalizedId);
			$adapter = new Zend_Auth_Adapter_OpenId($openid_identifier);
			$cache = Openid_Module::loadCache();
			$cacheId = 'id_' . Page_Module::urlToCacheId($normalizedId);
			if(!$cache->test($cacheId))
			{
				$adapter->setExtensions($sreg);
			}
			$result = $auth->authenticate($adapter);
			if(!$result->isValid())
			{
				$auth->clearIdentity();
				$this->_flashMessenger->setNamespace('error')->addMessage(
					sprintf($translate->_('OpenID %s is invalid.'),
					       $openid_identifier));
				$this->_redirect();
			}
		} else if(!empty($openid_mode)) {
			$cache = Openid_Module::loadCache();
			$id = $this->_request->getQuery('openid_claimed_id');
			$cacheId = 'id_' . Page_Module::urlToCacheId($id);
			$adapter = new Zend_Auth_Adapter_OpenId();
			if(!$identity = $cache->load($cacheId))
			{
				$adapter->setExtensions($sreg);
			}
			$result = $auth->authenticate($adapter);
			$id = $auth->getIdentity();
			switch($result->getCode())
			{
				case Zend_Auth_Result::SUCCESS:
					if(!$identity = $cache->load($cacheId))
					{
						$details = $sreg->getProperties();
						if(!empty($details))
						{
							$details['id'] = $id;
							$data = Openid_Module::sregToProperties($details);
							$openidTable = Doctrine::getTable('Openid');
							try
							{
								$openid = $openidTable->find($id);
								if(false === $openid)
								{
									$data['acl_role'] = 'member';
									$data['url'] = $id;
									$openid = new Openid();
								} else {
									unset($data['acl_role']);
								}
								$openid->merge($data);

								$aclRole = new Pivip_Auth_Identity_Property(
								           	'aclRole', $openid->acl_role);
								$properties = array();
								foreach($openid as $property => $value)
								{
									if(empty($value) || $value instanceof Doctrine_Null)
									{
										continue;
									}
									$properties[$property] =
										new Pivip_Auth_Identity_Property($property,
										                                 $value);
								}
								$uid = new Pivip_Auth_Identity_Property('uid', $id);
								$identity = new Pivip_Auth_Identity($uid,
								                                    $aclRole,
								                                    $properties['fn'],
								                                    $properties); 
								$openid->save();
							} catch(Exception $e) {
								$translate = Zend_Registry::get('Zend_Translate');
								$error = $translate->_(sprintf(
								         'Could not save OpenID %s to the database.',
								         $id));
								$logger = Zend_Registry::get('logger');
								$logger->err($error);
							}
							$cache->save($identity, $cacheId,
							             array('identity'));
						} else {
							$openidTable = Doctrine::getTable('Openid');
							$openid = $openidTable->find($id);
							$uid = new Pivip_Auth_Identity_Property('uid', $id);
							$aclRole = new Pivip_Auth_Identity_Property('aclRole',
								$openid->acl_role);
							$fn = new Pivip_Auth_Identity_Property('fn', $openid->fn);
							$properties = array();
							foreach($row as $k => $v)
							{
								if(!is_string($v))
								{
									continue;
								}
								$properties[$k] = new Pivip_Auth_Identity_Property($k,
								                                                   $v);
							}
							$identity = new Pivip_Auth_Identity($uid, $aclRole, $fn,
							                                    $properties);
						}
					}
					$storage = $auth->getStorage();
					$storage->write($identity);
					$this->_flashMessenger->resetNamespace()->addMessage(
						sprintf($translate->_('Logged in with OpenID %s.'),
						       $id));
					$this->_redirect();
					break;
				case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
					$this->_flashMessenger->setNamespace('error')->addMessage(
						sprintf($translate->_('OpenID %s is invalid.'),
						       $auth->getIdentity()));
					$auth->clearIdentity();
					$this->_redirect();
					break;
				case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
					$this->_flashMessenger->setNamespace('error')->addMessage(
						sprintf($translate->_('Could not authenticate OpenID %s.'),
						       $auth->getIdentity()));
					$auth->clearIdentity();
					$this->_redirect();
					break;
				default:
					$this->_flashMessenger->setNamespace('error')->addMessage(
						sprintf($translate->_('Problem logging in with OpenID %s.'),
						       $auth->getIdentity()));
					$auth->clearIdentity();
					$this->_redirect();
					break;
			}
		}
	}

	/**
	 * Logout
	 */
	public function logoutAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$translate = Zend_Registry::get('Zend_Translate');
		$this->_flashMessenger->resetNamespace()->addMessage(
			$translate->_('Logged out.'));
		$this->_redirect();
	}

	/**
	 * Display a login form/logout link in the menu section
	 */
	public function menuAction()
	{
		$auth = Zend_Auth::getInstance();
		if(!$auth->hasIdentity())
		{
			$this->view->openidForm = $this->_getForm();
		}
	}

	/**
	 * Display a login form in the main section
	 */
	public function mainAction()
	{
	}

	/**
	 * Display a login form/logout link in the sidebar
	 */
	public function sidebarAction()
	{
	}

	/**
	 * Display a login form/logout link in the footer
	 */
	public function asideAction()
	{
	}
}