<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Planet
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Planet module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Planet_Module extends Pivip_Module_Abstract
{
	/**
	 * Check whether the Planet tables exists
	 *
	 * @return boolean Whether the Planet module is installed 
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH
		                . 'application/models/generated/BasePlanetFeed.php') ||
		   !file_exists(CODE_PATH
		                . 'application/models/generated/BasePlanetPlanet.php'))
		{
			return false;
		}
		try
		{
			$feedTable = Doctrine::getTable('PlanetFeed');
			$planetTable = Doctrine::getTable('PlanetPlanet');
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * @return boolean False
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Install the Planet module
	 */
	public function install()
	{
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'planet');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Load the generic cache for use by this module
	 *
	 * Entries do not need to expire because they are (supposed to be) refreshed
	 * when the feed lifetime expires.
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache($prefix = 'planet')
	{
		$frontendOptions = array('cache_id_prefix' => 'planet_' . $prefix . '_',
		                         'automatic_serialization' => true);
		if('entry' == $prefix)
		{
			$frontendOptions['lifetime'] = null;
		}
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$cache = Zend_Cache::factory('Core',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $cacheConfig->backendOptions->toArray());
		return $cache;
	}

	/**
	 * @return array A list of the blocks this module provides for normal pages,
	 *               in the format:
	 *               array('section' => array('Link to add this block'
	                                          	=> 'Block name'))
	 */
	public static function getPageBlocks($location)
	{
		if(!self::isInstalled())
		{
			return array();
		}
		$translate = Zend_Registry::get('Zend_Translate');
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Planet_AddPlanet');
		$blocks = array();
		$sections = array('main');
		foreach($sections as $section)
		{
			$blocks[$section] = array($route->assemble(array('section' => $section,
			                                                 'location'
			                                                 	=> $location))
			                          	=> $translate->_('Planet'));
		}
		return $blocks;
	}

	/**
	 * Load the module
	 */
	public function bootstrap()
	{
		// Define the Access Control List
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('planet'))
		    ->allow('admin', 'planet', 'write');
		Zend_Registry::set('acl', $acl);
		// Define the routes
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$options = array('module' => 'planet', 'controller' => 'planet',
		                 'action' => 'feed');
		$route = new Zend_Controller_Router_Route(
		         	'planet/:block_id/feed/', $options);
		$router->addRoute('Planet_PlanetFeed', $route);
		$options = array('module' => 'planet', 'controller' => 'planet',
		                 'action' => 'add', 'location' => '');
		$route = new Zend_Controller_Router_Route(
		         	'planet/add/:section/:location', $options);
		$router->addRoute('Planet_AddPlanet', $route);
		$options = array('module' => 'planet', 'controller' => 'planet',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route('planet/edit/:planet_id',
		                                          $options);
		$router->addRoute('Planet_EditPlanet', $route);
		$options = array('module' => 'planet', 'controller' => 'planet',
		                 'action' => 'delete');
		$route = new Zend_Controller_Router_Route('planet/delete/:planet_id',
		                                          $options);
		$router->addRoute('Planet_DeletePlanet', $route);

		$options = array('module' => 'planet', 'controller' => 'feed',
		                 'action' => 'add');
		$route = new Zend_Controller_Router_Route(
		         	'planet/addfeed/:planet_id', $options);
		$router->addRoute('Planet_AddFeed', $route);
		$options = array('module' => 'planet', 'controller' => 'feed',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route('planet/editfeed/:feed_id',
		                                          $options);
		$router->addRoute('Planet_EditFeed', $route);
		$options = array('module' => 'planet', 'controller' => 'feed',
		                 'action' => 'delete');
		$route = new Zend_Controller_Router_Route('planet/deletefeed/:feed_id',
		                                          $options);
		$router->addRoute('Planet_DeleteFeed', $route);
		$options = array('module' => 'planet', 'controller' => 'entry',
		                 'action' => 'main');
		$route = new Zend_Controller_Router_Route('planet/viewentry/:guid',
		                                          $options);
		$router->addRoute('Planet_ViewEntry', $route);
	}
}