<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Planet
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Create a "Planet" of RSS feeds
 *
 * A "Planet" is an aggregation of RSS feeds from many different sources.
 */
class Planet_PlanetController extends Page_Abstract
{
	/**
	 * Generate a Planet feed
	 *
	 * Generate a Planet feed from the different sources and pass that on to the
	 * feed controller.
	 *
	 * @todo Allow changing the Planet settings such as the title
	 * @todo Refactor
	 */
	public function mainAction()
	{
		$planetId = $this->_request->getParam('block_id');
		if(null === $planetId)
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$settingsCache = Planet_Module::loadCache('settings');
		if(!$settings = $settingsCache->load($planetId))
		{
			$planetTable = Doctrine::getTable('PlanetPlanet');
			$settings = $planetTable->find($planetId);
			$settingsCache->save($settings, $planetId,
			                     array('planet', 'settings'));
		}
		$cache = Planet_Module::loadCache();
		if(!$rows = $cache->load($planetId))
		{
			$query = Doctrine_Query::create();
			$query->select('f.link')
			      ->from('PlanetFeed f')
			      ->where('f.block_id=?', $planetId);
			$rows = $query->execute();
			$cache->save($rows, $planetId, array('planet', 'feeds'));
		}
		$feedCache = Planet_Module::loadCache('feed');
		$entryCache = Planet_Module::loadCache('entry');
		$planetFeed = array();
		$entryRoute = Zend_Controller_Front::getInstance()->getRouter()
		              ->getRoute('Planet_ViewEntry');
		foreach($rows as $row)
		{
			$cacheId = $row->feed_id;
			if(!$feed = $feedCache->load($cacheId))
			{
				try
				{
					$imported = Zend_Feed::import($row->link);
				} catch(Exception $e) {
					Zend_Registry::get('logger')->err($e->getMessage());
					continue;
				}
				$feed = array();
				foreach($imported as $entry)
				{
					$entry = new Vogel_Feed_Entry_Unified($entry);
					$guid = $entry->id();
					$guid = Pivip_Utility_Cache::toCacheId($guid);
					$planetEntry = array('title' => $entry->title(),
					                     'link' => $entry->link(),
					                     'description' => $entry->description(),
					                     'content' => $entry->content(),
					                     'lastUpdate' => strtotime($entry->date()));
					$feed[] = $guid;
					$entryCache->save($planetEntry, $guid, array('planet', 'entry'));
					$planetFeed['entries'][] = $planetEntry;
				}
				$feedCache->save($feed, $cacheId, array('planet', 'feed'));
			} else {
				foreach($feed as $guid)
				{
					if(!$planetEntry = $entryCache->load($guid))
					{
						continue;
					}
					$planetFeed['entries'][] = $planetEntry;
				}
			}
		}
		if(!isset($planetFeed['entries']))
		{
			$planetFeed['entries'] = array();
		}
		usort($planetFeed['entries'], array($this, 'compareEntriesByDate'));
		$i = 0;
		$entries = array();
		foreach($planetFeed['entries'] as $k => $v)
		{
			if($settings->entries <= $i++)
			{
				break;
			}
			$entries[$k] = $v;
		}
		$planetFeed['entries'] = $entries;
		$planetFeed['title'] = $settings->title;
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Planet_PlanetFeed');
		$planetFeed['link'] = $this->_request->getBaseUrl() . '/'
		                      . $route->assemble(array('block_id' => $planetId));
		$planetFeed['charset'] = 'utf-8';
		$planetFeed = Zend_Feed::importArray($planetFeed);
		$this->view->feed = $planetFeed;
		$this->view->urlParams = array('planet_id' => $planetId);
		if($this->_isAllowed('edit'))
		{
			$this->view->allowEdit = true;
		}
		if($this->_isAllowed('delete'))
		{
			$this->view->allowDelete = true;
		}
	}

	/**
	 * Sort entries by date
	 */
	public function compareEntriesByDate($a, $b)
	{
		if(!isset($b['lastUpdate']))
		{
			return -1;
		} else if(!isset($a['lastUpdate'])) {
			return 1;
		}
		if($a['lastUpdate'] > $b['lastUpdate']) {
			return -1;
		} else if($a['lastUpdate'] < $b['lastUpdate']) {
			return 1;
		}
		return 0;
	}

	/**
	 * Add a planet
	 */
	public function addAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$location = '/' . $this->_request->getParam('location');
		if(!$this->_isAllowed('add'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to add a planet.'));
			$this->_redirect($location);
		}
		$section = $this->_request->getParam('section');
		if(empty($section))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You need to specify a section to add a planet to.'));
			$this->_redirect($location);
		}

		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);

		$form = $this->_getForm();
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$block = new Block();
			$block->PlanetPlanet->title = $form->getValue('title');
			$block->PlanetPlanet->entries = $form->getValue('entries');
			$block->location = $location;
			$block->action = $section;
			$block->controller = 'planet';
			$block->module = 'planet';
			$block->save();
			$cacheId = Page_Module::urlToCacheId($location);
			$cache = Page_Module::loadCache();
			$cache->remove($cacheId);
			$this->_flashMessenger->addMessage($translate->_(
				'Planet created, please add a feed.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate
			                                   ->_('Failed to add the planet.'));
			$this->_redirect($location);
		}
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Planet_AddFeed');
		$options = array('planet_id' => $block->block_id);
		$this->_redirect($route->assemble($options));
	}

	/**
	 * Edit a Planet
	 */
	public function editAction()
	{
		$planetId = $this->_request->getParam('planet_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $planetId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No planet specified.'));
			$this->_redirect('');
		}

		if(!$this->_isAllowed('edit'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to edit a planet.'));
			$this->_redirect($html->Block->location);
		}

		$feedsTable = Doctrine::getTable('PlanetFeed');
		$query = Doctrine_Query::create();
		$query->from('PlanetFeed f')
		      ->where('block_id=?', $planetId);
		$this->view->feeds = $query->execute();

		$this->view->planetId = $planetId;

		$planetTable = Doctrine::getTable('PlanetPlanet');
		$settings = $planetTable->find($planetId);
		$form = $this->_getForm();
		$form->submit->setLabel('Edit planet');
		$form->populate($settings->toArray());
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$settings->title = $form->getValue('title');
			$settings->entries = $form->getValue('entries');
			$settings->save();
			$cache = Planet_Module::loadCache('settings');
			$cache->remove($planetId);
			$this->_flashMessenger->addMessage($translate->_(
				'Planet updated.'));
			$this->_redirect($settings->Block->location);
		} catch(Exception $e) {
			$this->_refresh('Failed to update the planet.', 'error');
		}
	}

	/**
	 * Delete a planet
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$planetId = $this->_request->getParam('planet_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $planetId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No Planet specified.'));
			$this->_redirect('');
		}

		$settingsTable = Doctrine::getTable('PlanetPlanet');
		$settings = $settingsTable->find($planetId);
		$location = $settings->Block->location;

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to delete a planet.'));
			$this->_redirect($location);
		}

		try
		{
			$query = Doctrine_Query::create();
			$query->delete('PlanetFeed')
			      ->from('PlanetFeed')
			      ->where('block_id=?', $planetId)
			      ->execute();
			$settings->Block->delete();
			$cache = Planet_Module::loadCache('settings');
			$cache->remove($planetId);
			$cache = Planet_Module::loadCache();
			$cache->remove($planetId);
			$cache = Page_Module::loadCache();
			$cache->remove(Pivip_Utility_Cache::toCacheId($location));
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_flashMessenger->addMessage($translate->_(
				'There was an error deleting the planet.'));
			$this->_redirect($location);
		}
		$this->_flashMessenger->addMessage($translate->_(
			'The planet was deleted successfully.'));
		$this->_redirect($location);
	}

	/**
	 * @return Zend_Form The form to add an HTML block
	 */
	protected function _getForm()
	{
		$form = new Zend_Form;
		$form->setMethod('post')
		     ->setAction($this->_request->getRequestUri())
		     ->addElement('text', 'title')
		     ->title->setRequired(true)
		            ->setLabel('Planet title')
		            ->addValidator('NotEmpty');
		$form->addElement('text', 'entries')
		     ->entries->setLabel('Number of entries')
		              ->setValue(10);
		$form->addDisplayGroup(array('title', 'entries'), 'planet')
		     ->planet->setLegend('Planet');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Add planet')
		       ->addDecorator('HtmlTag', array('tag' => 'dd'))
		       ->removeDecorator('DtDdWrapper');
		$form->addElement($submit);
		return $form;
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges = null)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if('edit' == $privileges || 'add' == $privileges ||
		   'delete' == $privileges)
		{
			if(!$acl->isAllowed('guest', 'planet', 'write')
			   && !$auth->hasIdentity())
			{
				return false;
			}
			if(!$acl->isAllowed($identity->aclRole, 'planet', 'write'))
			{
				return false;
			}
		}
		if(!$acl->isAllowed('guest', 'block', $privileges) &&
		   !$auth->hasIdentity())
		{
			return false;
		}
		return $acl->isAllowed($identity->aclRole, 'block', $privileges);
	}
}