<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Install
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Install the Planet module
 */
class Planet_InstallController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Install the Planet module
	 */
	public function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		if(Planet_Module::isInstalled())
		{
			return;
		}
		if(!Pivip_Utility_Doctrine::updateModels('modules/planet/Tables.yml'))
		{
			return false;
		}
		try
		{
			Doctrine::createTablesFromModels(CODE_PATH . 'application/models');
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			return;
		}
		$translate = Zend_Registry::get('Zend_Translate');
		$this->_flashMessenger->addMessage($translate->_(
			'The Planet module was successfully installed.'));
		$this->_redirect('');
	}
}