<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Planet
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage feeds in planets
 */
class Planet_FeedController extends Page_Abstract
{
	/**
	 * Display the stream in the main section of the website
	 *
	 * @todo Allow for displaying of a feed that is not pre-configured
	 */
	public function mainAction()
	{
		$this->view->feed = $feed;
	}

	/**
	 * Render a feed as HTML
	 */
	public function feedAction()
	{
		$feed = $this->_request->getParam('feed');
		if(null === $feed)
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$feed = new Vogel_Feed_Unified($feed);
		$this->view->title = $feed->title();
		$this->view->link = $feed->link();
		$entries = array();
		foreach($feed as $entry)
		{
			$entry = new Vogel_Feed_Entry_Unified($entry);
			$entries[] = array('title' => $entry->title(),
			                   'content' => $entry->content(),
			                   'link' => $entry->link());
		}
		$this->view->entries = $entries;
		$this->view->setEscape('strip_tags');
	}

	/**
	 * Add a feed
	 */
	public function addAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		if(!$this->_isAllowed('add'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to add a feed to a planet.'));
			$this->_redirect('');
		}
		$planetId = $this->_request->getParam('planet_id');
		if(null === $planetId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No planet to add a feed to specified.'));
			$this->_redirect('');
		}

		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		$form = $this->_getForm();
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$feed = new PlanetFeed();
			$feed->block_id = $planetId;
			$feed->link = $form->getValue('link');
			try
			{
				$imported = Zend_Feed::import($feed->link);
				$feed->title = $imported->title();
			} catch(Exception $e) {
			}
			$feed->save();
			$cache = Planet_Module::loadCache();
			$cache->remove($planetId);
			$this->_helper->flashMessenger
			->addMessage($translate->_('Feed added.'));
			$this->_redirect($feed->Block->location);
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_refresh('Failed to add the feed.', 'error');
		}
	}

	/**
	 * Edit a feed
	 */
	public function editAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');

		$feedId = $this->_request->getParam('feed_id');

		if(null === $feedId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No feed specified.'));
			$this->_redirect();
		}

		$feedTable = Doctrine::getTable('PlanetFeed');
		$feed = $feedTable->find($feedId);
		$planetId = $feed->block_id;

		if(!$this->_isAllowed('edit'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to edit feeds.'));
			$this->_redirect($feed->Block->location);
		}

		$this->_helper->viewRenderer->setScriptAction('add');
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);

		$form = $this->_getForm();
		$form->submit->setLabel('Edit');
		$form->populate($feed->toArray());
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}

		try
		{
			$feed->link = $form->getValue('link');
			try
			{
				$imported = Zend_Feed::import($feed->link);
				$feed->title = $imported->title();
			} catch(Exception $e) {
			}
			$feed->save();
			$cache = Planet_Module::loadCache();
			$cache->remove($planetId);
			$cache = Planet_Module::loadCache('feed');
			$cache->remove($feedId);
			$this->_helper->flashMessenger
			->addMessage($translate->_('Feed edited.'));
			$this->_redirect($feed->Block->location);
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_refresh('Failed to edit the feed.', 'error');
		}

		try
		{
			$stream->service = $form->getValue('service');
			$stream->keywords = $form->getValue('keywords');
			$stream->amount = intval($form->getValue('amount'));
			$stream->type = $form->getValue('type');
			$stream->size = $form->getValue('size');
			$stream->save();
			$this->_flashMessenger->addMessage($translate->_('Block updated.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate->_(
				'An error occured while updating the block, please try again.'));
			$this->_redirect($this->_request->getRequestUri());
		}
		$cache = Imagestream_Module::loadCache();
		$cache->remove($streamId);
		$this->_redirect($html->Block->location);
	}

	/**
	 * Delete a feed
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$feedId = $this->_request->getParam('feed_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $feedId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No feed specified.'));
			$this->_redirect();
		}
		$feedsTable = Doctrine::getTable('PlanetFeed');
		$feed = $feedsTable->find($feedId);
		$location = $feed->Block->location;

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to delete a feed.'));
			$this->_redirect($location);
		}

		$planetId = $feed->block_id;
		$planetCache = Planet_Module::loadCache();
		$feedCache = Planet_Module::loadCache('feed');
		try
		{
			$feed->delete();
			$planetCache->remove($planetId);
			$feedCache->remove($feedId);
		} catch(Exception $e) {
			$logger = Zend_Registry::get('logger');
			$logger->err($e->getMessage());
			$this->_flashMessenger->setNamespace('error')
			->addMessage($translate->_(
				'An error occurred while deleting the feed, please try again.'));
			$this->_redirect($location);
		}
		$this->_flashMessenger->resetNamespace()->addMessage($translate->_(
			'The feed was successfully deleted.'));
		$this->_redirect($location);
	}

	/**
	 * @return Zend_Form The form to add an HTML block
	 */
	protected function _getForm()
	{
		$form = new Zend_Form;
		$form->setMethod('post')
		     ->setAction($this->_request->getRequestUri())
		     ->addElement('text', 'link')
		     ->link->setRequired(true)
		           ->setLabel('Link to feed')
		           ->addValidator('NotEmpty');
		$form->addDisplayGroup(array('link'), 'feed')
		     ->feed->setLegend('Feed');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Add feed')
		       ->addDecorator('HtmlTag', array('tag' => 'dd'))
		       ->removeDecorator('DtDdWrapper');
		$form->addElement($submit);
		return $form;
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges = null)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if('edit' == $privileges || 'add' == $privileges ||
		   'delete' == $privileges)
		{
			if(!$acl->isAllowed('guest', 'planet', 'write')
			   && !$auth->hasIdentity())
			{
				return false;
			}
			if(!$acl->isAllowed($identity->aclRole, 'planet', 'write'))
			{
				return false;
			}
		}
		if(!$acl->isAllowed('guest', 'block', $privileges) &&
		   !$auth->hasIdentity())
		{
			return false;
		}
		return $acl->isAllowed($identity->aclRole, 'block', $privileges);
	}
}