<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Planet
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage entries in planet feeds
 */
class Planet_EntryController extends Page_Abstract
{
	/**
	 * Display a feed entry and its comments
	 *
	 * Check whether a block_id has been set - if it has, find out which entry to
	 * display using that. If it has not, check whether the guid has been set,
	 * and if it has, push the default request onto the stack after displaying
	 * the entry.
	 *
	 * @todo Write this function
	 */
	public function mainAction()
	{
	}

	/**
	 * Display an entry
	 */
	public function entryAction()
	{
		$entryId = $this->_request->getParam('guid');
		if(null === $entryId)
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$cache = Planet_Module::loadCache('entry');
		$cacheId = Pivip_Utility_Cache::toCacheId($entryId);
		if(!$entry = $cache->load($cacheId))
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$entry = Vogel_Feed_Entry_Unified($entry);
		$this->view->title = $entry->title();
		$this->view->link = $entry->link();
		$this->view->content = $entry->content();
		$this->view->setEscape('strip_tags');
	}
}