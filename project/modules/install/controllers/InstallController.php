<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Install
 * @subpackage Controllers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the installation
 */
class Install_InstallController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Manage configuration of Pivip
	 *
	 * Displays a form that lets the administrator enter new configuration
	 * values and, when that form is submitted, writes the new configuration data
	 * to disk and redirects to the next step.
	 */
	public function configurationAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$this->view->headTitle($translate->_('Installation'));
		$generalConfig = Zend_Registry::get('generalConfig');
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('install');
		$action = $route->assemble(array('action' => 'configuration'));
		$dbName = new Zend_Form_Element_Text('database');
		$dbName->setLabel('Database name');
		$dbUser = new Zend_Form_Element_Text('username');
		$dbUser->setLabel('Username')
		       ->setRequired(true);
		$dbPassword = new Zend_Form_Element_Password('password');
		$dbPassword->setLabel('Password')
		           ->setRequired(true);
		$form = new Zend_Form();
		$form->setAction($action)
		     ->setMethod('post')
		     ->setAttrib('id', 'generalSettings')
		     ->setAttrib('class', 'install')
		     ->addElement($dbName)
		     ->addElement($dbUser)
		     ->addElement($dbPassword)
		     ->addDisplayGroup(array('database', 'username', 'password'), 'db')
		     ->db->setLegend('Inloggegevens database');
		$siteName = new Zend_Form_Element_Text('sitename');
		$siteName->setLabel('Name')
		         ->setRequired(true)
		         ->addFilter('HtmlEntities');
		$form->addElement($siteName)
		     ->addDisplayGroup(array('sitename'), 'site')
		     ->site->setLegend('Site details');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Next step');
		$form->addElement($submit);
		$data = $generalConfig->toArray();
		$data = $data['database']['params'];
		$data['sitename'] = $generalConfig->website->name;
		if($this->_request->isPost() &&
		   ('username' == trim($form->getValue('username')) ||
		    'password' == trim($form->getValue('password'))
		   ))
		{
			$notification = $translate->_('Please use a non-default' .
'username/password combination.');
			$this->_flashMessenger->addMessage($notification);
		} else if($this->_request->isPost() &&
		          !$form->isValid($this->_request->getPost()))
		{
			$notification = $translate->_(
				'Please fill in all form elements correctly');
			$this->_flashMessenger->addMessage($notification);
		} else if($this->_request->isPost()) {
			$generalConfig->database->params->database = $form->getValue('database');
			$generalConfig->database->params->username =
				$form->getValue('username');
			$generalConfig->database->params->password =
				$form->getValue('password');
			$generalConfig->website->name =
				$form->getValue('sitename');
			try
			{
				$generalConfig->save();
				$notification = $translate->_('Configuration saved.');
				$this->_flashMessenger($notification);
				$this->_redirect('');
			} catch(Exception $e) {
				$error = $translate->_('Could not save the configuration.
Is the configuration file writable?');
				$this->_flashMessenger->setNamespace('error')->addMessage($error);
			}
		}
		$form->populate($data);
		$this->view->form = $form;
	}

	/**
	 * Install the modules
	 *
	 * Will first check whether a specific module to install has been specified.
	 * If so, its installation will commence. If not, it will iterate over all
	 * modules and install the first one that is not installed yet.
	 */
	public function modulesAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$target = $this->_request->getParam('target');
		if(!empty($target))
		{
			$target = ucfirst(strtolower($target)) . '_Module';
			$target = new $target;
		} else {
			$modules = new Pivip_Modules();
			
			foreach($modules as $module)
			{
				if(!$module->isInstalled())
				{
					$target = $module;
					break;
				}
			}
		}

		if(empty($target))
		{
			$translate = Zend_Registry::get('Zend_Translate');
			$this->_flashMessenger->addMessage($translate->_(
				'Installation complete.'));
			$this->_redirect('');
		}

		$target->install();
	}
}