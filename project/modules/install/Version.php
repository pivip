<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Install
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Get the current version of the Pivip
 *
 * Since the Install module is an essential part of Pivip, it is tailored to
 * whatever version of Pivip is running (meaning that it has the same version
 * number).
 */
class Install_Version extends Pivip_Version
{
}