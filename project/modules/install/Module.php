<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Install
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage installation of Pivip
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Install_Module extends Pivip_Module_Abstract
{
	/**
	 * Checks whether Pivip is installed
	 *
	 * First checks whether the database password has been changed, then
	 * whether a connection with the database can be made.
	 *
	 * @return boolean Whether Pivip is already installed.
	 */
	public static function isInstalled()
	{
		$generalConfig = Zend_Registry::get('generalConfig');
		if(!isset($generalConfig->database->params->database) ||
		   !isset($generalConfig->database->params->username) ||
		   !isset($generalConfig->database->params->password) ||
		   'username' == $generalConfig->database->params->username ||
		   'password' == $generalConfig->database->params->password)
		{
			return false;
		}
		$options = $generalConfig->database->params->toArray();
		$dsn = Pivip_Utility_Doctrine::arrayToDsn($options);
		try
		{
			$connection = Doctrine_Manager::connection($dsn);
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * Communicate that Pivip needs configuration
	 *
	 * Always returns true since Pivip needs to be told the database settings.
	 *
	 * @return boolean True, Pivip needs to be configured
	 */
	public static function needsConfiguring()
	{
		return true;
	}

	/**
	 * Edit the configuration files, install modules.
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
	}

	/**
	 * Remove all Pivip-related tables from the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * Redirect to the Module installation page
	 */
	public function configure()
	{
	}

	/**
	 * Commence installation when Pivip isn't installed yet
	 */
	public function bootstrap()
	{
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController->getRouter();
		$route = new Zend_Controller_Router_Route('install/:action/*',
		                                          array('module' => 'install',
		                                                'controller' => 'install'));
		$router->addRoute('install', $route);
		$route = new Zend_Controller_Router_Route('install/module/:target/*',
		                                          array('module' => 'install',
		                                                'controller' => 'install',
		                                                'action' => 'modules',
		                                                'target' => ''));
		$router->addRoute('Install_InstallModule', $route);
		if(self::isInstalled())
		{
			return;
		}
		$nextRequest = new Zend_Controller_Request_Simple('configuration',
		                                                  'install', 'install');
		self::_pushStack($nextRequest);
	}
}