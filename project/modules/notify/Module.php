<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Notify
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Notify module
 *
 * The Notify module allows for displaying of notifications
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Notify_Module extends Pivip_Module_Abstract
{
	/**
	 * Notify needs no installation; returns true.
	 *
	 * @return boolean True, Notify need not be installed
	 */
	public static function isInstalled()
	{
		return true;
	}

	/**
	 * Communicate that Notify does not need any configuration
	 *
	 * @return boolean False, since Notify does not need to be configured.
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Load the controller
	 */
	public function bootstrap()
	{
		$nextRequest = new Zend_Controller_Request_Simple('notifications',
		                                                  'index',
		                                                  'notify');
		self::_pushStack($nextRequest);
	}
}