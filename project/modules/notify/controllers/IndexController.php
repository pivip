<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Page
 * @subpackage Controllers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Load a page's blocks
 */
class Notify_IndexController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Loads the blocks of a certain page
	 *
	 * @throws Page_Exception
	 */
	public function notificationsAction()
	{
		$this->view->notificationTypes = array('error', 'warning', 'tip',
		                                       'information');
		foreach($this->view->notificationTypes as $type)
		{
			$$type = $this->_flashMessenger->setNamespace($type)->getMessages();
			if('information' == $type)
			{
				$general = $this->_flashMessenger->resetNamespace()->getMessages();
				$$type = array_merge($$type, $general);
			}
			foreach($$type as $notification)
			{
				$this->view->placeholder($type)->set($notification);
			}
		}
	}
}