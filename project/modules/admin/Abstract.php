<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Admin
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Modules can extend this to provide an administrative interface
 */
abstract class Admin_Abstract extends Pivip_Controller_Module_Abstract
{
	/**
	 * Check whether the user is allowed to acess the administrative interface
	 */
	protected function _isAllowed()
	{
		$acl = Zend_Registry::get('acl');
		$auth = Pivip_Auth::getInstance();
		if(!$auth->hasIdentity())
		{
			return false;
		}
		$identity = $auth->getIdentityProperties();
		return $acl->isAllowed((string) $identity->aclRole, 'administration');
	}

	/**
	 * Check whether the visitor has enough rights and add the Admin menu
	 */
	public function init()
	{
		if(!$this->_isAllowed())
		{
			$translate = Zend_Registry::get('Zend_Translate');
			$this->_helper->flashMessenger->addMessage($translate->_(
				'You do not have enough rights to access the administrative
interface.'));
			$this->_redirect('');
		}
		$nextRequest = new Zend_Controller_Request_Simple('pagecontext', 'config',
		                                                  'admin');
		$this->_helper->actionStack($nextRequest);
	}

	/**
	 * Modules can override this function to display an overview
	 *
	 * When overridden, this will be called to display an overview at the main
	 * administration page. Should not be used when not needed since it can
	 * clutter the interface.
	 */
	public function overview()
	{
	}
}