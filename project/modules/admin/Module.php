<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Admin
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Admin module
 *
 * The Admin module provides an administrative interface to Pivip and modules.
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Admin_Module extends Pivip_Module_Abstract
{
	/**
	 * Checks whether the Admin module is installed
	 *
	 * @return boolean True, the Admin module needs no installation.
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseBlock.php'))
		{
			return false;
		}
		$cache = Page_Module::loadCache();		
		if(!$rows = $cache->load('core'))
		{
			try
			{
				$query = new Doctrine_Query();
				$query->from('Block b')
				      ->where('b.location=?', 'core')
				      ->orderby('b.priority ASC');
				$rows = $query->execute();
			} catch(Exception $e) {
				return false;
			}
		}
		foreach($rows as $row)
		{
			if('admin' == $row->module &&
			   'config' == $row->controller &&
			   'nav' == $row->action &&
			   'core' == $row->location)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Communicate that Admin does not need configuration
	 *
	 * Always returns false.
	 *
	 * @return boolean false, Admin does not need to be configured
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Adds the 'nav' block to the 'default' bundle.
	 *
	 * @throws Pivip_Install_Exception
	 */
	public function install()
	{
		if($this->isInstalled())
		{
			return;
		}
		$nextRequest = new Zend_Controller_Request_Simple('index',
	                                                     'install',
	                                                     'admin');
		$this->_pushStack($nextRequest);
	}

	/**
	 * Add the ACL rules, install if not installed
	 */
	public function bootstrap()
	{
		if(Install_Module::isInstalled() && Page_Module::isInstalled() &&
		   !$this->isInstalled())
		{
			$this->install();
		}
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('administration'))
		    ->allow('admin', 'administration');
		Zend_Registry::set('acl', $acl);
		$options = array('module' => 'admin', 'controller' => 'config',
		                 'action' => 'view', 'option' => 'index');
		$route = new Zend_Controller_Router_Route('admin/view/:name/:option', $options);
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$router->addRoute('Admin_ViewModule', $route);
	}
}