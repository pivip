<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Admin
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Install the Admin module
 */
class Admin_InstallController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Install the Admin module
	 *
	 * Add the menu link.
	 */
	public function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		if(!Page_Module::isInstalled())
		{
			return false;
		}
		$translate = Zend_Registry::get('Zend_Translate');
		try
		{
			$block = new Block();
			$block->location = 'core';
			$block->action = 'nav';
			$block->controller = 'config';
			$block->module = 'admin';
			$block->save();
			$cache = Page_Module::loadCache();
			$cache->remove('core');
		} catch(Exception $e) {
			$error = $translate->_(
				'Could not add a link to the Administration area.');
			$this->_flashMessenger->setNamespace('error')->addMessage($error);
			return;
		}
		$this->_refresh('The Admin module was installed successfully.');
	}
}