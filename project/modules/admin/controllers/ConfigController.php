<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Admin
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Provides an administration interface
 */
class Admin_ConfigController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Check whether the user is allowed to acess the administrative interface
	 */
	protected function _isAllowed()
	{
		$acl = Zend_Registry::get('acl');
		$auth = Pivip_Auth::getInstance();
		if(!$auth->hasIdentity())
		{
			return false;
		}
		$identity = $auth->getIdentityProperties();
		return $acl->isAllowed((string) $identity->aclRole, 'administration');
	}

	/**
	 * Display the admin interface home
	 *
	 * Will display an introductory talk about the administration interface,
	 * populate the sidebar with links to the configuration of every module,
	 * and finally make sure an overview is displayed of every module that
	 * supports overviews (the modules need to implement Admin_Abstract) and is
	 * enabled by the administrators.
	 *
	 * @todo Display overviews
	 */
	public function indexAction()
	{
		if(!$this->_isAllowed())
		{
			$translate = Zend_Registry::get('Zend_Translate');
			$this->_flashMessenger->addMessage($translate->_(
				'You do not have enough rights to access the administrative
interface.'));
			$this->_redirect();
		}
		try
		{
			$directories = new DirectoryIterator('modules');
		} catch(Exception $e) {
			// Fail silently, no modules loaded
			return;
		}
		foreach($directories as $directory)
		{
			if(!$directory->isDir() || $directory->isDot())
			{
				continue;
			}
			if(!file_exists('modules/' . $directory->getFileName() . '/Admin.php'))
			{
				continue;
			}
			$className = $directory->getFileName() . '_Admin';
		}
		$nextRequest = new Zend_Controller_Request_Simple('pagecontext',
		                                                  'config',
		                                                  'admin');
		$this->_helper->actionStack->pushStack($nextRequest);
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack->pushStack($defaultRequest);
	}

	/**
	 * Display a module's configuration interface
	 *
	 * Will forward to a module's configuration interface. Will default to
	 * calling its IndexAction(), can be overridden with the option-parameter
	 * (see route Admin_ViewModule). If the required parameters aren't set, it
	 * will foward to self::indexAction().
	 */
	public function viewAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		require_once 'modules/admin/Abstract.php';
		$module = $this->_request->getParam('name');
		if(null === $module)
		{
			return;
		}
		$module = strtolower($module);
		$adminControllerFile = 'modules/' . $module
		                       . '/controllers/AdminController.php';
		if(!file_exists($adminControllerFile))
		{
			return;
		}
		$action = $this->_request->getParam('option');
		if(null === $action)
		{
			$action = 'index';
		}
		$params = $this->_request->getParams();
		$nextRequest = new Zend_Controller_Request_Simple($action,
			                                               'admin',
			                                               $module,
			                                               $params);
		$this->_helper->actionStack($nextRequest);
	}

	/**
	 * Add links to all Module configuration pages
	 */
	public function pagecontextAction()
	{
		$this->view->addHelperPath('modules/admin/views/helpers',
		                           'Admin_View_Helper');

		$modules = new Pivip_Modules();
		
		foreach($modules as $module)
		{
			if(method_exists($module, 'configure'))
			{
				$moduleName = str_replace('_Module', '', get_class($module));
				$this->view->confList()->add($moduleName);
			}
		}
	}

	/**
	 * Add a link to the menu
	 */
	public function navAction()
	{
		$auth = Pivip_Auth::getInstance();
		if(!$auth->hasIdentity())
		{
			return;
		}
		$identity = $auth->getIdentityProperties();
		$acl = Zend_Registry::get('acl');
		if(!$acl->isAllowed((string) $identity->aclRole, 'administration'))
		{
			return;
		}
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('default');
		$link = $this->_request->getBaseUrl() . '/' .
			$route->assemble(array('module' => 'admin', 'controller' => 'config'));
		$translate = Zend_Registry::get('Zend_Translate');
		$value = $translate->_('Administer');
		$this->view->addNav($link, $value);
	}
}