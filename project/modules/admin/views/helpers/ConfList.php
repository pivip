<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Admin
 * @subpackage ViewHelpers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Create a list of modules to configure
 */
class Admin_View_Helper_ConfList
{
	protected $_list = '';

	public $view;

	public function setView($view)
	{
		$this->view = $view;
	}

	/**
	 * Manipulate this object
	 *
	 * @return Navigation_View_Helper_ConfList
	 */
	public function confList()
	{
		return $this;
	}

	/**
	 * Add an item to the list
	 *
	 * @param string $module Name of the module to add to the list
	 */
	public function add($module)
	{
		$params = array('name' => $module);
		$li = '
	<li>
		<a href="' . $this->view->url($params, 'Admin_ViewModule') . '"
		   title="' . $this->view->translate('Configure the %s module.', $module)
		            . '">' . $module . '</a>
	</li>';
		$this->_list .= $li;
	}

	/**
	 * Retrieve the list
	 *
	 * @return string The list
	 */
	public function __toString()
	{
		if(!empty($this->_list))
		{
			return '
<div class="block admin">
<h1>' . $this->view->translate('Configure modules') . '
</h1>
<ul class="admin modules">' . $this->_list . '
</ul>
</div>';
		}
		return $this->_list;
	}
}