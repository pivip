<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  PivipModulesDefault
 * @package   Module_Html
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Manage the Html module
 *
 * @see /library/Pivip/Module/Abstract.php
 */
class Html_Module extends Pivip_Module_Abstract
{
	/**
	 * Checks whether this module is already installed
	 *
	 * In the case of the Html module, "installed" means that the database table
	 * exists.
	 *
	 * @return boolean Whether Html is already installed.
	 */
	public static function isInstalled()
	{
		if(!file_exists(CODE_PATH . 'application/models/generated/BaseHtml.php'))
		{
			return false;
		}
		try
		{
			$htmlTable = Doctrine::getTable('Html');
		} catch(Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * Communicate that Html does not need any configuration
	 *
	 * @return boolean False, since Html does not need to be configured.
	 */
	public static function needsConfiguring()
	{
		return false;
	}

	/**
	 * Creates the table in the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
	}

	/**
	 * Remove the table from the database
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * Load the generic cache for use by this module
	 *
	 * @return Zend_Cache
	 */
	public static function loadCache()
	{
		$frontendOptions = array('cache_id_prefix' => 'html_');
		$cacheConfig = Zend_Registry::get('cacheConfig');
		$cache = Zend_Cache::factory('Core',
		                             $cacheConfig->cache->backend,
		                             $frontendOptions,
		                             $cacheConfig->backendOptions->toArray());
		return $cache;
	}

	/**
	 * @return array A list of the blocks this module provides for normal pages,
	 *               in the format:
	 *               array('section' => array('Link to add this block'
	                                          	=> 'Block name'))
	 */
	public static function getPageBlocks($location)
	{
		if(!self::isInstalled())
		{
			return array();
		}
		$translate = Zend_Registry::get('Zend_Translate');
		$route = Zend_Controller_Front::getInstance()->getRouter()
		         ->getRoute('Html_AddHtml');
		$blocks = array();
		$sections = array('main');
		foreach($sections as $section)
		{
			$blocks[$section] = array($route->assemble(array('section' => $section,
			                                                 'location'
			                                                 	=> $location))
			                          	=> $translate->_('Plain HTML block'));
		}
		return $blocks;
	}

	/**
	 * Load the module
	 */
	public function bootstrap()
	{
		// Define the Access Control List
		$acl = Zend_Registry::get('acl');
		$acl->add(new Zend_Acl_Resource('html'))
		    ->allow('admin', 'html', 'write');
		Zend_Registry::set('acl', $acl);
		// Define the routes
		$router = Zend_Controller_Front::getInstance()->getRouter();
		$options = array('module' => 'html', 'controller' => 'index',
		                 'action' => 'add', 'location' => '');
		$route = new Zend_Controller_Router_Route('html/add/:section/:location',
		                                          $options);
		$router->addRoute('Html_AddHtml', $route);
		$options = array('module' => 'html', 'controller' => 'index',
		                 'action' => 'edit');
		$route = new Zend_Controller_Router_Route('html/edit/:html_id',
		                                          $options);
		$router->addRoute('Html_EditHtml', $route);
		$options = array('module' => 'html', 'controller' => 'index',
		                 'action' => 'delete');
		$route = new Zend_Controller_Router_Route('html/delete/:html_id',
		                                          $options);
		$router->addRoute('Html_DeleteHtml', $route);
	}
}