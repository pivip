<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license    http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category   PivipModulesDefault
 * @package    Module_Html
 * @subpackage Controllers
 * @copyright  (C) 2008 Vincent Tunru
 * @author     Vincent Tunru <email@vincentt.org>
 */

/**
 * Load an HTML block
 */
class Html_IndexController extends Pivip_Controller_Module_Abstract
{
	/**
	 * Load an HTML block
	 */
	public function mainAction()
	{
		$htmlId = $this->_request->getParam('block_id');
		if(null === $htmlId)
		{
			$this->_helper->viewRenderer->setNoRender();
			return;
		}
		$cache = Html_Module::loadCache();
		if(!$code = $cache->load($htmlId))
		{
			$row = Doctrine::getTable('Html')->find($htmlId);
			$code = $row->html;
			$cache->save($code, $htmlId, array('html', 'block'));
		}
		$filter = new Vogel_Filter_HtmlPurifier();
		$this->view->setEscape(array($filter, 'filter'));
		$this->view->code = $code;
		$this->view->urlParams = array('html_id' => $htmlId);
		if($this->_isAllowed('edit'))
		{
			$this->view->allowEdit = true;
		}
		if($this->_isAllowed('delete'))
		{
			$this->view->allowDelete = true;
		}
	}

	/**
	 * Add an HTML block
	 */
	public function addAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');
		$location = '/' . $this->_request->getParam('location');
		if(!$this->_isAllowed('add'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to add a new HTML block.'));
			$this->_redirect($location);
		}
		$section = $this->_request->getParam('section');
		if(empty($section))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You need to specify a section to add HTML to.'));
			$this->_redirect($location);
		}
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);
		$form = $this->_getForm();
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$block = new Block();
			$block->location = $location;
			$block->action = $section;
			$block->controller = 'index';
			$block->module = 'html';
			$block->Html->html = $form->getValue('html');
			$block->save();
			$cacheId = Page_Module::urlToCacheId($location);
			$cache = Page_Module::loadCache();
			$cache->remove($cacheId);
			$this->_flashMessenger->addMessage($translate->_('Block added.'));
		} catch(Exception $e) {die($e->getMessage());
			$this->_flashMessenger->setNamespace('error')->addMessage($translate
			                                   ->_('Failed to add the block.'));
		}
		$this->_redirect($location);
	}

	/**
	 * Edit an HTML block
	 */
	public function editAction()
	{
		$translate = Zend_Registry::get('Zend_Translate');

		$htmlId = $this->_request->getParam('html_id');

		if(null === $htmlId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No HTML block specified.'));
			$this->_redirect();
		}

		$htmlTable = Doctrine::getTable('Html');
		$html = $htmlTable->find($htmlId);

		if(!$this->_isAllowed('edit'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to edit HTML blocks.'));
			$this->_redirect($html->Block->location);
		}

		$this->_helper->viewRenderer->setScriptAction('add');
		$defaultRequest = Zend_Registry::get('defaultRequest');
		$this->_helper->actionStack($defaultRequest);

		$form = $this->_getForm();
		$form->submit->setLabel('Edit');
		$form->block->setLegend('Edit block');
		$form->populate($html->toArray());
		$this->view->form = $form->render();
		if(!$this->_request->isPost() ||
		   !$form->isValid($this->_request->getPost()))
		{
			return;
		}
		try
		{
			$html->html = $form->getValue('html');
			$html->save();
			$this->_flashMessenger->addMessage($translate->_('Block updated.'));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate->_(
				'An error occured while updating the block, please try again.'));
			$this->_redirect($this->_request->getRequestUri());
		}
		$cache = Html_Module::loadCache();
		$cache->remove($htmlId);
		$this->_redirect($html->Block->location);
	}

	/**
	 * Delete an HTML block
	 *
	 * @todo Allow the user to undo this
	 */
	public function deleteAction()
	{
		$htmlId = $this->_request->getParam('html_id');
		$translate = Zend_Registry::get('Zend_Translate');

		if(null === $htmlId)
		{
			$this->_flashMessenger->addMessage($translate->_(
				'No HTML block specified.'));
			$this->_redirect();
		}

		$htmlTable = Doctrine::getTable('Html');
		$html = $htmlTable->find($htmlId);

		if(!$this->_isAllowed('delete'))
		{
			$this->_flashMessenger->addMessage($translate->_(
				'You are not allowed to delete blocks.'));
			$this->_redirect($html->Block->location);
		}

		$location = $html->Block->location;
		try
		{
			$html->Block->delete();
			$html->delete();
			$this->_flashMessenger->addMessage($translate->_(
				'Block deleted.'));
			$cache = Html_Module::loadCache();
			$cache->remove($htmlId);
			$cache = Page_Module::loadCache();
			$cache->remove(Page_Module::urlToCacheId($location));
		} catch(Exception $e) {
			$this->_flashMessenger->setNamespace('error')->addMessage($translate->_(
				'An error occurred while deleting the block, please try again.'));
		}
		$this->_redirect($location);
	}

	/**
	 * @return Zend_Form The form to add an HTML block
	 */
	protected function _getForm()
	{
		$form = new Zend_Form;
		$html = new Zend_Form_Element_Textarea('html');
		$html->setRequired(true)
		     ->setLabel('HTML');
		$form->addElement($html)
		     ->setMethod('post')
		     ->setAction($this->_request->getRequestUri())
		     ->addDisplayGroup(array('html'), 'block')
		     ->block->setLegend('Add block');
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Add')
		       ->addDecorator('HtmlTag', array('tag' => 'dd'))
		       ->removeDecorator('DtDdWrapper');
		$form->addElement($submit);
		return $form;
	}

	/**
	 * @param $privileges What the user needs to be allowed to do to blocks
	 * @return bool Whether the user has sufficient rights
	 */
	protected function _isAllowed($privileges = null)
	{
		$auth = Pivip_Auth::getInstance();
		$acl = Zend_Registry::get('acl');
		$identity = $auth->getIdentityProperties();
		if('edit' == $privileges || 'add' == $privileges ||
		   'delete' == $privileges)
		{
			if(!$acl->isAllowed('guest', 'html', 'write') && !$auth->hasIdentity())
			{
				return false;
			}
			if(!$acl->isAllowed($identity->aclRole, 'html', 'write'))
			{
				return false;
			}
		}
		if(!$acl->isAllowed('guest', 'block', $privileges) &&
		   !$auth->hasIdentity())
		{
			return false;
		}
		return $acl->isAllowed($identity->aclRole, 'block', $privileges);
	}
}