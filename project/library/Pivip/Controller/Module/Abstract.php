<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * 
 */
class Pivip_Controller_Module_Abstract extends Zend_Controller_Action
{
	/**
	 * Set the view script to the one associated with the current module
	 *
	 * @param Zend_Controller_Request_Abstract $request The request object
	 * @param Zend_Controller_Response_Abstract $response The response object
	 * @param array $invokeArgs Any additional invocation arguments
	 * @return void
	 */
	public function __construct(Zend_Controller_Request_Abstract $request,
	                            Zend_Controller_Response_Abstract $response,
	                            array $invokeArgs = array())
	{
		parent::__construct($request, $response, $invokeArgs);
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->_logger = Zend_Registry::get('logger');
		$parts = explode('_', get_class($this));
		$moduleName = strtolower($parts[0]);
		$this->_logger->setEventItem('module', $moduleName);
	}

	/**
	 * Redirect the visitor to the current page
	 *
	 * @param string $message A message to display to the user.
	 */
	protected function _refresh($message = null, $namespace = null)
	{
		if(!empty($message))
		{
			$translate = Zend_Registry::get('Zend_Translate');
			if(empty($namespace))
			{
				$this->_flashMessenger->resetNamespace()
				->addMessage($translate->_($message));
			} else {
				$this->_flashMessenger->setNamespace($namespace)
				->addMessage($translate->_($message));
			}
		}
		$this->_redirect($this->_request->getRequestUri(),
		                 array('prependBase' => false));
	}
}