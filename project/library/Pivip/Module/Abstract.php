<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Modules can extend this to provide an interface to the module
 */
abstract class Pivip_Module_Abstract implements Pivip_Module_Interface
{
	/**
	 * A list of required modules and their versions
	 *
	 * The module should define minimum and maximum versions, using
	 * 	array('module' => array('min' => 0.0.0dev', 'max' => '3.1.1'));
	 *
	 * @var array
	 */
	protected $_dependencies;

	/**
	 * Install the module (prepare the database, mark installed status, ...)
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether the installation succeeded
	 */
	public function install()
	{
	}

	/**
	 * Uninstall the module (remove database values, ...)
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean Whether uninstallation succeeded
	 */
	public function uninstall()
	{
	}

	/**
	 * Check whether the module's API dependencies are available
	 *
	 * @throws Pivip_Install_Exception
	 * @return boolean True if API dependencies are met.
	 */
	public function checkDependencies()
	{
	}

	/**
	 * Load the module
	 */
	public function bootstrap()
	{
	}

	/**
	 * Allows child classes to push a request onto the stack
	 *
	 * @param Zend_Controller_Request_Abstract $request The request to push.
	 */
	protected static function _pushStack(Zend_Controller_Request_Abstract $request)
	{
		$frontController = Zend_Controller_Front::getInstance();
		$actionStack = $frontController
		               	->getPlugin('Zend_Controller_Plugin_ActionStack');
		if(!$actionStack)
		{
			$actionStack = new Zend_Controller_Plugin_ActionStack();
			$frontController->registerPlugin($actionStack);
		}
		$actionStack->pushStack($request);
	}
}