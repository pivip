<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Pivip_Module_Abstract implements Pivip_Module_Interface
 */
interface Pivip_Module_Interface
{
	/**
	 * Modules must be able to determine whether they're installed
	 *
	 * If a module does not need to be installed, this function should return
	 * true by default.
	 *
	 * @return boolean True if the module is already installed, flase if not.
	 */
	public static function isInstalled();

	/**
	 * Whether a module needs configuration before installation
	 *
	 * Note: if modules return true, they also need to provide a
	 * configure() function.
	 *
	 * @return boolean Whether the module needs to be configured.
	 */
	public static function needsConfiguring();
}