<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Pivip Doctrine utility
 */
class Pivip_Utility_Doctrine extends Vogel_Utility_Doctrine
{
	/**
	 * Update existing models with a new model
	 *
	 * @param string $newModel Path to YAML schema file for the new model
	 * @param string $target   Path to the existing models to be updated
	 * @param array  $options  Options for model generation
	 */
	public static function updateModels($newModel, $target = null,
	                                    array $options = array())
	{
		if(null === $target)
		{
			$target = CODE_PATH . 'application/models';
		}
		return parent::updateModels($newModel, $target, $options);
	}
}