<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Pivip Cache utility
 */
class Pivip_Utility_Cache extends Pivip_Utility_Abstract
{
	/**
	 * Normalize a URL so it can be used as a cache ID
	 *
	 * @param  string $url URL to be cache ID-ized
	 * @return string Cache ID-ized URL
	 * @todo   Consider moving this to a filter
	 */
	public static function toCacheId($url)
	{
		return str_replace(':', '__colon__',
		       str_replace('.', '__fullstop__',
		       str_replace(';', '__semicolon__',
		       str_replace('%', '__percent__',
		       str_replace('/', '__fwdslash__',
		       str_replace('\\', '__bwdslash__',
		       str_replace('#', '__hash__',
		       str_replace('?', '__qmark__',
		       str_replace('&', '__amp__',
		       str_replace('-', '__hyphen__',
		       str_replace('=', '__equal__',
		       Zend_Filter::get($url, 'HtmlEntities'))))))))))));
	}
}