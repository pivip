<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Get the current version of Pivip or modules
 */
class Pivip_Version extends Pivip_Version_Abstract
{
	/**
	 * @var array Modules to be checked
	 */
	protected static $_modules;

	/**
	 * Retrieve Pivip's current version
	 *
	 * @return string Pivip's current version
	 */
	public static function getVersion()
	{
		return '0.0.0dev';
	}

	/**
	 * Load an instance of a subclass of Pivip_Version_Abstract
	 *
	 * @param  string                 $module Name of the module to check
	 * @return Pivip_Version_Abstract Instance of the module
	 */
	protected static function _loadInstance($module)
	{
		$module .= 'Version';
		if(!isset(self::$_modules[$module]))
		{
			self::$_modules[$module] = new $module;
		}
		if(!is_subclass_of(self::$_modules[$module], 'Pivip_Version_Abstract'))
		{
			throw new InvalidArgumentException();
		}
		return self::$_modules[$module];
	}

	/**
	 * Compare the given version with the version of a module
	 *
	 * @see    version_compare()
	 * @param  string $version Version to compare with (e.g. '3.1.1')
	 * @param  string $module  Name of a module to check the version of. If null,
	 *                         compareVersion() will compare with Pivip's version
	 * @return int -1 if the given version is older,
	 *              0 is versions are equal,
	 *              1 if the given version is higher than the current version.
	 */
	public static function compareVersion($version, $module = null)
	{
		if(null === $module)
		{
			$moduleVersion = $this->getVersion();
		} else {
			$module = self::_loadInstance($module);
			$moduleVersion = $module->getVersion();
		}
		return version_compare($version, $moduleVersion);
	}

	/**
	 * Find out which API a module is compatible with
	 *
	 * This function will find out whether a module has specified a specific API
	 * it is compatible with, and if not, it will return the name and version of
	 * the module itself.
	 *
	 * @return array The API the module is compatible with, and a minimum and
	 *               maximum version.
	 */
	public static function getApiCompatibility($module)
	{
		$vars = get_class_vars($module . 'Version');
		if(isset($vars['api']['name']) &&
		   isset($vars['api']['min']) &&
		   isset($vars['api']['max']))
		{
			$vars['api']['name'] = strtolower($vars['api']['name']);
			return $vars['api'];
		} else {
			$moduleInstance = self::_loadInstance($module);
			$api['name'] = strtolower($module);
			$api['min'] = $api['max'] = $moduleInstance->getVersion();
			return $api;
		}
	}

	/**
	 * Check whether a module is compatible with a certain API
	 *
	 * By default, this module will compare with the module's own name and
	 * version, but often module developers will want to override it when their
	 * API hasn't changed for several versions, or when they really are
	 * compatible with another module's API.
	 *
	 * @see    version_compare()
	 * @param  string $module  Name of a module the compatibility of which is to
	 *                         be tested.
	 * @param  string $api Name of the API's module.
	 * @param  string $highestVersion Highest version of the API's module.
	 * @param  string $lowestVersion Lowest version of the API's module.
	 *                               Optional, defaults to $highestVersion.
	 * @return boolean Whether this module is compatible with the API.
	 */
	public static function checkCompatibility($module, $api, $highestVersion,
	                                          $lowestVersion = null)
	{
		$moduleApi = self::getApiCompatibility($module);
		if(strtolower($api) == $moduleApi['name'] &&
		   	(($highestVersion > $moduleApi['min'] &&
		   	  $highestVersion < $moduleApi['max']) ||
		   	 (null !== $lowestVersion && $lowestVersion < $moduleApi['max'] &&
		   	  $lowestVersion > $moduleApi['min'])))
		{
			return true;
		}
		return false;
	}
}