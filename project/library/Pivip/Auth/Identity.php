<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Retrieve a visitor's details
 *
 * When a visitor logs in, the authentication method can save the visitor's
 * details in this object which can then be retrieved whenever a module needs
 * them.
 *
 * @link http://www.ietf.org/rfc/rfc2426.txt RFC 2426, vCard MIME Directory
 *                                           profile.
 */
class Pivip_Auth_Identity
{
	/**
	 * @var array Identity properties
	 */
	private $_properties;

	/**
	 * Add the required data (id, fn) and, optionally, additional data
	 *
	 * @param Pivip_Auth_Identity_Property $uid        Identifier, mostly the key
	 *                                                 used in the database.
	 * @param Pivip_Auth_Identity_Property $aclRole    Role (used to determine
	 *                                                 rights in ACL).
	 * @param Pivip_Auth_Identity_Property $fn         Name of the person this
	 *                                                 identity belongs to.
	 *                                                 Examples:
	 *                                                 "Drs. P."
	 *                                                 "Heinz Hermann Polzer".
	 * @param array                        $properties Any additional properties.
	 * @throws InvalidArgumentException
	 */
	public function __construct(Pivip_Auth_Identity_Property $uid,
	                            Pivip_Auth_Identity_Property $aclRole,
	                            Pivip_Auth_Identity_Property $fn,
	                            array $properties)
	{
		$this->_properties = array('uid' => $uid, 'aclRole' => $aclRole,
		                           'fn' => $fn);
		foreach($properties as $key => $property)
		{
			if($property instanceof Pivip_Auth_Identity_Property)
			{
				$this->_properties[$key] = $property;
			}
		}
	}

	/**
	 * Convert the class to a string
	 *
	 * @return string String representation of this class
	 */
	public function __toString()
	{
		return $this->_properties['uid']->value;
	}

	/**
	 * Retrieve identity properties
	 *
	 * @param  string $property Name of the property to retrieve.
	 * @return Pivip_Auth_Identity_Property The property 
	 */
	public function __get($property)
	{
		if('acl_role' == $property)
		{
			$property = 'aclRole';
		}
		return $this->_properties[$property];
	}

	/**
	 * Set a property of the identity
	 *
	 * @param  string                       $property Name of the property to set
	 * @param  Pivip_Auth_Identity_Property $value    Value of the property
	 * @return Pivip_Auth_Identity_Property The property
	 */
	public function __set($property, Pivip_Auth_Identity_Property $value)
	{
		$this->_properties[$property] = $value;
		return $this->_properties[$property];
	}

	/**
	 * Check whether a property is set
	 *
	 * @param  string  $property Name of the property to check
	 * @return boolean Whether the property is set.
	 */
	public function __isset($property)
	{
		if('acl_role' == $property)
		{
			$property = 'aclRole';
		}
		return isset($this->_properties[$property]);
	}

	/**
	 * Unset a property
	 *
	 * @param  string $property Name of the property to unset
	 * @return void
	 */
	public function __unset($property)
	{
		if('acl_role' == $property)
		{
			$property = 'aclRole';
		}
		unset($this->_properties[$property]);
	}
}