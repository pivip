<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Property of an identity
 *
 * @link http://www.ietf.org/rfc/rfc2426.txt RFC 2426, vCard MIME Directory
 *                                           profile.
 * @see /library/Pivip/Auth/Identity/Abstract.php
 * @todo  Provide the alias "key" for "pubkey"
 */
class Pivip_Auth_Identity_Property implements SeekableIterator, Countable
{
	/**
	 * @var array Property subproperties
	 */
	private $_subProperties = array();

	/**
	 * @var string Value of the property
	 */
	public $value;

	/**
	 * @var string Name of the property
	 */
	public $name;

	/**
	 * Set a property, as well as optional sub-properties
	 *
	 * @param string $property      The property to set
	 * @param mixed  $subProperties Provide an array to set sub-properties,
    *                              provide a string to just set the value
	 */
	public function __construct($property, $subProperties)
	{
		$this->name = strtolower($property);
		$this->_subProperties = new ArrayObject();

		if('email' == $property)
		{
			if(is_string($subProperties))
			{
				$this->value = $subProperties;
				$this->_subProperties = array('type' => 'internet',
				                              'value' => $subProperties);
			} else if(is_array($subProperties)) {
				if(empty($subProperties['type']))
				{
					$subProperties['type'] = 'internet';
				}
				$subProperties['type'] = strtolower($subProperties['type']);
				if(empty($subProperties['value']))
				{
					$values = $subProperties;
					unset($values['type']);
					$subProperties['value'] = implode('', $values);
				}
				$this->_subProperties = $subProperties;
				$this->value = $subProperties['value'];
			} else {
				throw new InvalidArgumentException();
			}
			return;
		}

		if('tel' == $property)
		{
			if(is_string($subProperties))
			{
				$this->value = $subProperties;
				$this->_subProperties = array('type' => 'voice',
				                              'value' => $subProperties);
			} else if(is_array($subProperties))
			{
				if(empty($subProperties['type']))
				{
					$subProperties['type'] = 'voice';
				}
				$subProperties['type'] = strtolower($subProperties['type']);
				if(empty($subProperties['value']))
				{
					$values = $subProperties;
					unset($values['type']);
					$subProperties['value'] = implode('', $values);
				}
				$this->_subProperties = $subProperties;
				$this->value = $subProperties['value'];
			} else {
				throw new InvalidArgumentException();
			}
			return;
		}

		if('adr' == $property)
		{
			$components = array('post-office-box', 'extended-address',
			                    'street-address', 'locality', 'region',
			                    'postal-code', 'country-name', 'type');
			if(is_string($subProperties))
			{
				$this->value = $subProperties;
				$values = explode(';', $subProperties);
				foreach($values as $i => $subProperty)
				{
					$this->_subProperties[$components[$i]] = $subProperty;
				}
			} else if(is_array($subProperties))
			{
				$value = '';
				foreach($components as $component)
				{
					if(!empty($subProperties[$component]))
					{
						$value .= $subProperties[$component];
					}
					$value .= ';';
				}
				$this->value = $value;
				$this->_subProperties = $subProperties;
			} else {
				throw new InvalidArgumentException();
			}
			return;
		}

		if('geo' == $property)
		{
			if(is_string($subProperties))
			{
				if(false === strpos($subProperties, ';'))
				{
					throw new InvalidArgumentException();
				}
				$values = explode(';', $subProperties);
				$this->value = $subProperties;
				$this->_subProperties = array('latitude' => $values[0],
				                              'longitude' => $values[1]);
			} else if(is_array($subProperties))
			{
				if(empty($subProperties['latitude']) ||
				   empty($subProperties['longitude']))
				{
					throw new InvalidArgumentException();
				}
				$this->value = $subProperties['latitude'] . ';' .
				               $subProperties['longitude'];
				$this->_subProperties = $subProperties;
			} else {
				throw new InvalidArgumentException();
			}
			return;
		}

		if('org' == $property)
		{
			if(is_string($subProperties))
			{
				$this->value = $subProperties;
				$this->_subProperties = array('organization-name' =>
				                              	$subProperties);
			} else if(is_array($subProperties))
			{
				if(empty($subProperties['organization-name']))
				{
					$subProperties['organization-name'] = implode('',
					                                              $subProperties);
				}
				$this->_subProperties = $subProperties;
				$this->value = $subProperties['organization-name'];
			} else {
				throw new InvalidArgumentException();
			}
			return;
		}

		if(is_string($subProperties))
		{
			$this->value = $subProperties;
		} else if(is_array($subProperties)) {
			$this->_subProperties = $subProperties;
			$this->value = implode(';', $subProperties);
		} else {
			throw new InvalidArgumentException();
		}
	}

	/**
	 * Return the value of this property
	 *
	 * @return string Value of the property
	 */
	public function __toString()
	{
		return $this->value;
	}

	/**
	 * Retrieve a sub-property
	 *
	 * @param  string $subProperty Name of the sub-property to retrieve.
	 * @return string Value of the sub-property 
	 */
	public function __get($subProperty)
	{
		if(isset($this->_subProperties[$subProperty]))
		{
			return $this->_subProperties[$subProperty];
		}
		return '';
	}

	/**
	 * Set a sub-property
	 *
	 * @param  string $subProperty Name of the sub-property to set.
	 * @param  string $value Value of the sub-property
	 */
	public function __set($subProperty, $value)
	{
		if(!is_string($subProperty) || !is_string($value))
		{
			throw new InvalidArgumentException();
		}
		$this->_subProperties[$subProperty] = $value;
	}

	/**
	 * Check whether a sub-property is set
	 *
	 * @param  string  $subProperty Name of the sub-property to check
	 * @return boolean Whether the sub-property is set.
	 */
	public function __isset($subProperty)
	{
		return isset($this->_subProperties[$subProperty]);
	}

	/**
	 * Unset a sub-property
	 *
	 * @param string $subProperty Name of the sub-property to unset
	 * @return void
	 */
	public function __unset($subProperty)
	{
		unset($this->_subProperties[$subProperty]);
	}

	/**
	 * The current value
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 */
	public function current()
	{
		return current($this->_subProperties);
	}

	/**
	 * The current key
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 */
	public function key()
	{
		return key($this->_subProperties);
	}

	/**
	 * Move the pointer forward
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 */
	public function next()
	{
		next($this->_subProperties);
	}

	/**
	 * Rewind the pointer
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 */
	public function rewind()
	{
		reset($this->_subProperties);
	}

	/**
	 * Get a specific value
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 * @param mixed $index Index of the value to retrieve
	 */
	public function seek($index)
	{
		return $this->__get($index);
	}

	/**
	 * Check whether the current index is valid
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceSeekableIterator.html
	 */
	public function valid()
	{
		return (false !== current($this->_subProperties));
	}

	/**
	 * Count the number of subproperties
	 *
	 * @link http://www.php.net/~helly/php/ext/spl/interfaceCountable.html
	 */
	public function count()
	{
		return count($this->_subProperties);
	}
}