<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Pivip authentication class
 */
class Pivip_Auth extends Zend_Auth
{
	/**
	 * @var null|Pivip_Auth Singleton pattern implementation
	 */
	protected static $_instance = null;

	/**
	 * Singleton pattern implementation
	 */
	private function __construct()
	{}

	/**
	 * Singleton pattern implementation
	 */
	private function __clone()
	{}

	/**
	 * Singleton pattern implementation
	 *
	 * @return Pivip_Auth
	 */
	public static function getInstance()
	{
		if(null == self::$_instance)
		{
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	/**
	 * Retrieve the identity as a string from storage
	 *
	 * @return string|null null when no identity is available
	 */
	public function getIdentity()
	{
		$storage = $this->getStorage();

		if($storage->isEmpty())
		{
			return null;
		}

		$identity = $storage->read();
		if($identity instanceof Pivip_Auth_Identity)
		{
			$identity = $identity->__toString();
		}
		return $identity;
	}

	/**
	 * Retrieve the identity from storage
	 *
	 * @return mixed|null null when no identity is available
	 */
	public function getIdentityProperties()
	{
		$storage = $this->getStorage();

		if($storage->isEmpty())
		{
			return null;
		}
		return $storage->read();
	}
}