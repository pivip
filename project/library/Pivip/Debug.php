<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Pivip debugging utility class
 */
class Pivip_Debug extends Zend_Debug
{
	/**
	 * @var string The current setup (i.e. "dev" or "production")
	 */
	protected static $_setup = 'production';

	/**
	 * Retrieve the current setup
	 *
	 * @return string The current setup
	 */
	public static function getSetup()
	{
		$configuration = new Zend_Config_Ini('data/config/general.ini', 'developers');
		switch($configuration->setup)
		{
			case 'dev':
				// Break intentionally omitted
			case 'devel':
				self::$_setup = 'dev';
				break;
			case 'live':
				// Break intentionally omitted
			case 'production':
				// Break intentionally omitted
			default:
				self::$_setup = 'production';
				break;
		}
		return self::$_setup;
	}

	/**
	 * Find out whether we're currently in a testing environment
	 *
	 * @return bool True when in a testing environment
	 */
	public static function isTesting()
	{
		if('dev' == self::getSetup())
		{
			return true;
		}
		return false;
	}
}