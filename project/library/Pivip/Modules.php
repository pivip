<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Iterates through available modules, acting as an interface to each
 *
 * @link http://nl3.php.net/spl Standard PHP Library function reference
 * @link http://www.php.net/~helly/php/ext/spl/ Standard PHP Library docs
 */
class Pivip_Modules extends ArrayIterator
{
	/**
	 * Set up Pivip_Modules
	 *
	 * Load a list of available modules using DirectoryIterator and feeds the
	 * respective module classes as an Iterator to parent::__construct().
	 */
	public function __construct()
	{
		try
		{
			$directories = new DirectoryIterator('modules');
		} catch(Exception $e) {
			// Fail silently, no modules loaded
			return;
		}
		$modules = array();
		foreach($directories as $directory)
		{
			if(!$directory->isDir() || $directory->isDot())
			{
				continue;
			}
			if(!file_exists('modules/' . $directory->getFileName() . '/Module.php'))
			{
				continue;
			}
			$className = $directory->getFileName() . '_Module';
			$modules[] = new $className;
		}
		parent::__construct($modules);
	}
}