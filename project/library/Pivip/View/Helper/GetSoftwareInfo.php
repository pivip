<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Retrieve the name of the software
 */
class Pivip_View_Helper_GetSoftwareInfo
{
	/**
	 * Retrieve information about the software
	 *
	 * @param string $name Information to retrieve
	 * @param mixed  The requested information
	 */
	public function __get($name)
	{
		switch($name)
		{
			case 'name':
				return 'Pivip';
				break;
			default:
				return;
		}
	}

	/**
	 * Return an instance of this class of which information can be extracted
	 */
	public function getSoftwareInfo()
	{
		return new self;
	}
}