<?php

/**
 * Pivip
 * Copyright (C) 2008  Vincent Tunru

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 *
 * @license   http://www.fsf.org/licensing/licenses/info/GPLv2.html GPL v.2
 * @category  Pivip
 * @package   Pivip
 * @copyright (C) 2008 Vincent Tunru
 * @author    Vincent Tunru <email@vincentt.org>
 */

/**
 * Secure output
 */
class Pivip_View_Helper_Secure
{
	protected $_view;

	public function setView($view)
	{
		$this->_view = $view;
	}

	/**
	 * Correct input
	 */
	public function secure($input, $filter = null, array $args = array(),
	                       array $namespaces = array())
	{
		if(null === $input)
		{
			return $this->view->escape($input);
		}
		$namespaces[] = 'Pivip_Filter';
		if(Zend_Loader::isReadable('Vogel/Filter'))
		{
			$namespaces[] = 'Vogel_Filter';
		}
		return Zend_Filter::get($input, $filter, $args, $namespaces);
	}
}